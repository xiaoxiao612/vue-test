(function ($) {
    var methods = {
        //饼图
        chartsPie: function (url) {
            // var dom = document.getElementById("main");
            var dom = this[0];
            // console.log(dom);
            var myChart = echarts.init(dom);
            var app = {};
            option = null;
            app.title = '环形图';
            option = {
                title: {
                    text: '数据模块分类占比',
                    x: 'center',
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    x: 'right',
                    y:'center',
                    data: [],
                },
                series: [
                    {
                        name:'访问来源',
                        type:'pie',
                        radius: ['50%', '70%'],
                        avoidLabelOverlap: false,
                        label: {
                            normal: {
                                show: true,
                                // position: 'center'
                            },
                            // emphasis: {
                            //     show: true,
                            //     textStyle: {
                            //         fontSize: '30',
                            //         fontWeight: 'bold'
                            //     }
                            // }
                        },
                        labelLine: {
                            normal: {
                                show: false,
                            }
                        },
                        itemStyle:{
                            normal:{
                                label:{
                                    show: true,
                                    formatter: '{d}%'
                                },
                                labelLine :{show:true}
                            }
                        },
                        data: []
                    }
                ]
            };
            if (option && typeof option === "object") {
                myChart.setOption(option, true);
            }
            //获取json数据动态加载
            $.get(url).done(function (data) {


                // 填入数据
                // var seriesData = [];
                // $.each(data.data,function (key,values) {
                //     var obj = new Object();
                //     obj.value = values.value;
                //     obj.name = values.name;
                //     seriesData.push(obj);
                // });
                // console.log(seriesData);
                // console.log(data.data);
                // console.log(data.legend.data);

                myChart.setOption({
                    legend: {
                        data:data.pie.legend.data,
                    },
                    series: [{
                        // 根据名字对应到相应的系列
                        data: data.pie.data
                    }]
                });
            });
        },
        //折线图
        chartLine: function (url) {
            // var dom = document.getElementById("polygonal");
            var dom = this[0];
            var myChart = echarts.init(dom);
            var app = {};
            option = null;
            option = {
                title: {
                    text: '基本信息',
                    x: 'center',
                },
                xAxis: {
                    type: 'category',
                    data: [],
                },
                yAxis: {
                    type: 'value',
                },
                series: [{
                    data: [],
                    type: 'line'
                }]
            };
            if (option && typeof option === "object") {
                myChart.setOption(option, true);
            }
            $.get(url).done(function (data) {

                // 填入数据
                // console.log(data.data);
                myChart.setOption({
                    xAxis: {
                        data: data.line.xAxis.data
                    },
                    series: [{
                        // 根据名字对应到相应的系列
                        data: data.line.data
                    }]
                });
            });
        }
    };
    //方法
    $.fn.myCharts = function (method){
        // 方法调用
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method' + method + 'does not exist on jQuery.myCharts');
        }
    }
})(jQuery);

// console.log($("#main")[0]);
