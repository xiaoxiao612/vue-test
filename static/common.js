/**
 * @Company  亚信科技（中国）有限公司
 * @Copyright  @2014亚信科技（中国）有限公司 服务电话：15920375955
 * @author kebai
 * @date 2015-12-03 
 */

//公共模块的构造函数方法

var Com = new Common();

function Common() {

    var that = this; //保存this变量

    /** 
     * title: 常用的表单验证  
     * @author kebai
     * @date 2016-1-1
     **/
    that.isNumber = function(val) {
        var reg = /^[\d|\.|,]+$/;
        return reg.test(val);
    };
    that.isInt = function(val) {
        if (val === "") {
            return false;
        }
        var reg = /\D+/;
        return !reg.test(val);
    };
    that.isEmail = function(email) {
        var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return reg.test(email);
    };
    that.isMobile = function(mobile) {
        var reg = /1[34578]{1}\d{9}$/;
        return reg.test(mobile);
    };
    that.isTel = function(tel) {
        var reg = /^\d{3}-\d{8}|\d{4}-\d{7}$/; //只允许使用数字-空格等
        return reg.test(tel);
    };
    that.isJson = function(obj) { //是否是json
        var isjson = obj && typeof(obj) == "object" && Object.prototype.toString.call(obj).toLowerCase() == "[object object]";
        return isjson;
    };
    that.isArray = function(array) { //是否是数组
        return Object.prototype.toString.call(array) === '[object Array]';
    };

    /*数组最大值*/
    that.arrMax = function(array) {
        return Math.max.apply(Math, array);
    };
    /*数组最小值 */
    that.arrMin = function(array) {
        return Math.min.apply(Math, array);
    };
    /*数组求和 */
    that.arrSum = function(array) {
        var sum = 0,
            num;
        for (var i = 0; i < array.length; i++) {
            num = parseInt(array[i]) || 0;
            sum += num;
        }
        return sum;
    };

    /*删除数组 */
    that.arrDel = function(arr, val, n) {
        //arr:数组,val：数组值,n:索引(val,n二选一)
        val = val || null;
        if (!n && n != 0) {
            n = -1;
        }
        var index = $.inArray(val, arr);
        if (index >= 0) {
            n = index;
        }
        if (n < 0) { //n第几项，从0开始　　
            return arr;　
        } else {　　
            return arr.slice(0, n).concat(arr.slice(n + 1, arr.length));
        }
    };

    /*合并两个一般数组并去重 */
    that.merge = function(arr1, arr2) {
        var result;
        if (that.isArray(arr1) && that.isArray(arr2)) {
            result = $.merge($.merge([], arr1), arr2);
            result = that.unique(result);
            return result;
        }
    };

    /*根据id去重合并两个json数组*/
    that.mergeJsonArr = function(arrOld, arrNew, id) {
        id = id || 'id';
        var flag, result = arrOld;
        $.each(arrNew, function(i, value) {
            flag = true;
            if (!value) {
                return;
            }
            $.each(arrOld, function(j, value2) {
                if (value2 && value[id] == value2[id]) {
                    flag = false; //判断是否已存在
                }
            });
            if (flag) {
                result.push(value);
            }
        });
        return result;
    };

    /*json数组去重*/
    that.unique = function(arr) {
        if (that.isEmpty(arr)) {
            return [];
        }
        var temp = [],
            result = [];
        if (that.isArray(arr) && arr.length > 0) {

            if (!that.isJson(arr[0])) { //一般数组去重
                return $.unique(arr);
            }

            $.each(arr, function(i, value) {
                temp.push(that.toJson(value, false)); //转字符串
            });

            $.each($.unique(temp), function(i, value) {
                result.push(that.toJson(value)); //转json
            });
            return result;
        }

    };

    /*在数组中添加值，若已存在则不添加,(value若为json则通过id判断,addKey数值字段存在则增加1)*/
    that.arrAdd = function(arr, value, id, addkey, addValue) {
        id = id || 'id';
        addValue = Number(addValue) || 1;
        if (that.isEmpty(value)) {
            return arr;
        } else if (that.isEmpty(arr)) {
            arr = [];
        }
        var flag = false;
        if (addkey) {
            value[addkey] = addValue;
        }
        if (arr.length == 0) {
            arr.push(value);
        } else {
            $.each(arr, function(i, value2) {
                if (value == value2 && !that.isJson(value) || !that.isEmpty(value[id]) && value[id] == value2[id] || that.toJson(value, false) == that.toJson(value2, false)) {
                    flag = true;
                    if (addkey) { //数值增加
                        var num = Number(value2[addkey]) || 0;
                        value2[addkey] = num + addValue;
                    }
                }
            });
            if (!flag) {
                arr.push(that.toJson(value));
            }
        }
        return arr;
    };
    /*获取json数组id项/某字段值的集合*/
    that.getArrKey = function(arr, key) {
        if (that.isEmpty(arr)) {
            arr = [];
        }
        var newArr = [];
        $.each(arr, function(i, value) {
            if (!that.isEmpty(value[key])) {
                newArr.push(value[key]);
            }
        });
        return newArr;
    };

    /*数据是否为空*/
    that.isEmpty = function(data) {

        var dataStr = $.trim(data); //去空格并转成字符串(ture/false/0)

        if (that.isArray(data) && data.length == '0') {
            return true;
        } else if (that.isJson(data) && isEmptyObject(data)) {
            return true;
        } else if (!dataStr) {
            return true;
        } else {
            return false;
        }
        //json是否空对象
        function isEmptyObject(obj) {
            for (var n in obj) {
                return false;
            }
            return true;
        }
    };

    /*根据key字段,删除json以及json数组某个键值对，会直接在原json数据上删除*/
    that.jsonDel = function(json, key) {

        if (that.isEmpty(json) || that.isEmpty(key)) {
            return json;
        }
        if (!that.isArray(key)) {
            key = key.split(',');
        }
        if (that.isArray(json)) {
            $.each(json, function(i, value) {
                del(value);
            });
        } else {
            del(json);
        }

        function del(json) {
            $.each(key, function(i, value) {
                delete json[value];
            });
        }
        return json;
    };
    /*根据key和对应的值，删除它在json数组的json值*/
    that.jsonArrDel = function(json, key, val) {

        if (that.isEmpty(json) || that.isEmpty(key) || that.isEmpty(val)) {
            return json;
        }
        if (!that.isArray(val)) {
            val = val.split(',');
        }

        if (that.isArray(json)) {
            $.each(val, function(i, value) {
                $.each(json, function(i, value2) {
                    if (value == value2[key]) {
                        json = that.arrDel(json, value2);
                    }
                });
            });

        }
        return json;
    };


    /** 
     * title: doT模板引擎插件 
     * obj：指定对象
     * data：输出的数据
     * tmpl：对象的模版，默认obj+'-tmpl'
     * callback：回调函数，默认为空 
     * flag: 默认0替换,1末尾追加,2首位追加
     * @author kebai
     * @date 2015-12-03
     **/
    that.tmpl = function(obj, data, tmpl, callback, flag) {
        var $obj = $(obj);
        if ($obj.length == '0') {
            return;
        }
        if (that.isEmpty(data)) { data = {}; }
        flag = flag || 0;
        callback = callback || '';
        if (!tmpl) {
            tmpl = $obj.selector ? '#' + $obj.selector.replace(/#|\./g, '') + '-tmpl' : '#' + obj.replace(/#|\./g, '') + '-tmpl';
        }
        var $objTmpl = $(tmpl).html();
        var dataHtml = doT.template($objTmpl).apply(null, [data]);
        dataHtml = dataHtml.replace(/>\s*undefined/ig, '>'); //去掉undefined
        if (flag == 0) {
            $obj.html(dataHtml);
        } else if (flag == 1) {
            $obj.append(dataHtml);
        } else if (flag == 2) {
            $obj.prepend(dataHtml);
        }
        callback && callback($obj, dataHtml);
    };

    /** 
     * title: 定义图表宽高
     * obj：指定对象 
     * callback：回调函数，默认为空
     * @author kebai
     * @date 2015-12-03
     **/
    that.ratio = function(obj, callback) {
        var $obj = $(obj);
        callback = callback || null;
        if ($obj.length == '0') {
            return;
        }
        var objH, objW, HH;
        objH = $obj.parent().height();
        objW = $obj.parent().width();
        HH = objH > objW ? objW : objH; //按最小值计算     
        $obj.css({
            "height": HH,
            "width": "100%"
        });
        callback && callback(objH, objW);
    };



    /** 
     * title: 对javascript、html标签实体转义 
     * data:数据源字符串
     * @author kebai
     * @date 2017-4-7
     **/

    that.xss = function(data) { //防止<script  type="text/javascript">与html标签执行
        if (that.isEmpty(data) || typeof(data) != "string") {
            return data;
        }
        var reg = /<[A-Za-z]+>/g,
            str = data.toLowerCase().replace(/\s+|\//g, '');
        if (str.indexOf('<#if') != -1 || str.indexOf('<graph') != -1 || str.indexOf('<?xml') != -1) { //sql代码、拓扑图、xml
            data = data.replace(/<script/ig, '&lt;script');
            return data;
        }
        if (str.indexOf('<script') != -1 || reg.test(str) || str.indexOf('onload') != -1 || str.indexOf('src=') != -1) {
            data = data.replace(/</g, '&lt;').replace(/>/g, '&gt;');
        }
        return data;
    };


    /** 
     * title: Ajax请求数据
     * obj：指定对象
     * url：接口路径
     * callback：接口请求回调函数
     * dataJson：传参数据，post方法时用 
     * type：请求方式类型，默认get方式请求 
     * async：请求类型，默认异步请求      
     * dataType：返回数据类型，默认为json 
     * flag: 是否返回接口报错信息，默认false不返回
     * @author kebai
     * @date 2015-12-03
     **/
    that.ajaxData = function(obj, url, callback, dataJson, type, async, dataType, flag) {
        var $obj = $(obj);
        if ($obj.length == '0') {
            return;
        }
        dataJson = dataJson || '';
        type = type || 'GET';
        dataType = dataType || 'json';
        async = async === false ? false : true;
        flag = flag || false;

        if (dataJson) { //防止xss攻击
            for (var i in dataJson) {
                dataJson[i] = that.xss(dataJson[i]);
            }
        }

        $.ajax({
            url: url,
            data: dataJson,
            dataType: dataType,
            type: type,
            async: async,
            cache: false,
            success: function(result) {
                callback($obj, result, url, dataJson);
                $obj.find('.as-loading-tips').html('');
            },
            error: function(response, textStatus) {
                $obj.find('.as-loading-tips').html('');
                $('.as-dialog-text').html(''); //对话框加载提示文本 
                if (flag) {
                    callback('error', response); //返回错误无提示
                } else {
                    var msg = '';
                    if (textStatus == 'parsererror') {
                        msg = '接口返回的数据格式不是' + dataType;
                        dialog && that.dialogTipsMsg(msg, true);
                        console && console.log(url + '==>' + msg);
                        return;
                    } else if (response.status == 500 && textStatus == 'error') {
                        msg = '服务繁忙,请稍候再试！';
                        dialog && that.dialogTipsMsg(msg, true);
                        console && console.log(url + '==>' + msg);
                        return;
                    } else if (response.status == '0' && textStatus == 'error') {
                        msg = '网络已断开！';
                        dialog && that.dialogTipsMsg(msg, true);
                        console && console.log(url + '==>' + msg);
                        return;
                    } else if (textStatus == 'error') {
                        msg = '请求出错了！';
                        dialog && that.dialogTipsMsg(msg, true);
                        console && console.log(url + '==>' + msg);
                        return;
                    }
                }
            }
        });
    };

    /** 
     * title: Ajax请求Html、XML数据
     * @author kebai
     * @date 2016-5-19
     **/
    that.ajaxDataHtml = function(obj, url, callback, dataJson, type, async, dataType, flag) {
        dataType = dataType || "html";
        that.ajaxData(obj, url, callback, dataJson, type, async, dataType, flag);
    };

    /** 
     * title: 两个、三个、四个、五个Ajax请求数据，全部成功后才返回数据(数据顺序一一对应)
     * jsonArr: ajax请求的json数组[{url:url,dataType:"json"},{url:url,dataType:"json"}]
     * dataType: 类型要传否则容易报错（格式错误后也会成功返回）
     * callback：接口请求回调函数  
     * flag: 是否返回接口报错信息，默认false不返回
     * @author kebai
     * @date 2016-7-1
     **/
    that.whenData = function(jsonArr, callback, flag) {
        flag = flag || false;
        var len = jsonArr.length;
        switch (len) {
            case 2:
                $.when($.ajax(jsonArr[0]), $.ajax(jsonArr[1])).done(function(data1, data2) {
                    callback(data1[0], data2[0]);
                }).fail(function() {
                    fnerror();
                });
                break;
            case 3:
                $.when($.ajax(jsonArr[0]), $.ajax(jsonArr[1]), $.ajax(jsonArr[2])).done(function(data1, data2, data3) {
                    callback(data1[0], data2[0], data3[0]);
                }).fail(function() {
                    fnerror();
                });
                break;
            case 4:
                $.when($.ajax(jsonArr[0]), $.ajax(jsonArr[1]), $.ajax(jsonArr[2]), $.ajax(jsonArr[3])).done(function(data1, data2, data3, data4) {
                    callback(data1[0], data2[0], data3[0], data4[0]);
                }).fail(function() {
                    fnerror();
                });
                break;
            case 5:
                $.when($.ajax(jsonArr[0]), $.ajax(jsonArr[1]), $.ajax(jsonArr[2]), $.ajax(jsonArr[3]), $.ajax(jsonArr[4])).done(function(data1, data2, data3, data4, data5) {
                    callback(data1[0], data2[0], data3[0], data4[0], data5[0]);
                }).fail(function() {
                    fnerror();
                });
                break;
        }

        function fnerror() {
            if (flag) {
                callback('error');
            } else if (typeof dialog === 'function') {
                that.dialogTipsMsg('服务繁忙或者网络已断开,请稍候再试！', true);
                return;
            }
        }

    };


    /** 
     * title: 验证url是否有效(只能是同域名下有效否则都是flase)
     * url: 文件路径 
     * callback: 回调函数
     * timeout: 超时时间,默认永不超时
     * @author kebai
     * @date 2017-08-29
     **/

    that.validUrl = function(url, callback, async, timeout) {
        var flag; //-1(超时)、0(无效)、1(有效)
        url = that.xss(decodeURI(url));
        url = url.replace(/^(\s+|#|\?)*/, ''); //去掉开始的特殊符号 
        if (url.length == '0') { return; }
        timeout = timeout || 0;
        async = async || false;
        var ajaxTimeout = $.ajax({
            url: url,
            type: 'GET',
            async: async, //必须为true超时才生效
            timeout: timeout,
            complete: function(response, status) {
                if (status == 'timeout') { //超时,status还有success,error等值的情况
                    flag = -1; //超时
                    ajaxTimeout.abort(); //停止请求
                } else {
                    if (response.status == 200) {
                        flag = 1; //有效
                    } else {
                        flag = 0; //无效
                    }
                }
                callback && callback(response, status, flag);
            }
        });
    };

    /** 
     * title: 图表显示设置
     * obj：指定对象，必须为id
     * data：图表数据
     * themeStyle：图表主题 
     * @author kebai
     * @date 2015-12-03
     **/
    that.chartSet = function(obj, data, themeStyle) {
        themeStyle = themeStyle || '';
        if ($(obj).length == '0') {
            return 0;
        }
        that.ratio(obj); //设置对象的宽高 
        var docObj = document.getElementById(obj.replace('#', ''));
        var myChart = echarts.init(docObj, themeStyle);
        myChart.setOption(data);
        $(window).off('resize').on('resize', function() {
            that.ratio(obj);
            myChart.setTheme(themeStyle);
            myChart.resize();
        });
        return myChart;
    };

    /** 
     * title: 字符长度获取
     * str：字符串
     * noInt：长度是否取整，默认向上取整,有小数就整数部分加1  
     * @author kebai
     * @date 2015-12-03
     **/
    that.getLength = function(str, noInt) {
        if (str == 'undefined' || str == null || str.length == '0') {
            return 0;
        }
        str = str.toString();
        noInt = noInt || false;
        var char_length = 0,
            son_char;
        for (var i = 0; i < str.length; i++) {
            son_char = str.charAt(i);
            encodeURI(son_char).length > 2 ? char_length += 1 : char_length += 0.5;
            //如果是汉字，长度大于2 ，一个字=2个字符
        }
        if (!noInt) {
            char_length = Math.ceil(char_length);
        }
        return char_length;
    };


    /** 
     * title: 时间、日期对象的格式化 
     * date：毫秒时间
     * format：事件格式"yyyy-MM-dd hh:mm:ss"，年-月-日 时：分：秒
     * @author kebai
     * @date 2015-12-03
     **/
    that.dateFormat = function(date, format) {
        //eg:format="yyyy-MM-dd hh:mm:ss";
        format = format || "yyyy-MM-dd hh:mm:ss";
        date = new Date(Number(date));
        var o = {
            "M+": date.getMonth() + 1, // month
            "d+": date.getDate(), // day
            "h+": date.getHours(), // hour
            "m+": date.getMinutes(), // minute
            "s+": date.getSeconds(), // second
            "q+": Math.floor((date.getMonth() + 3) / 3), // quarter
            "S": date.getMilliseconds()
        };
        if (/(y+)/.test(format)) {
            format = format.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(format)) {
                format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
            }
        }
        return format;
    };

    /** 
     * title: 日期追加时间
     * time：时间（yyyy/MM/dd,yyyy/MM/dd hh:mm:ss,yyyy-MM-dd hh:mm:ss）
     * days：追加天数
     * format：返回时间格式
     * @author kebai
     * @date 2016-8-10
     **/
    that.dateAddTime = function(time, days, format) {
        if (time.indexOf('-') != -1) {
            time = time.replace(/\-/g, '/');
        }
        var newTime = new Date(time).getTime() + days * 24 * 60 * 60 * 1000;

        newTime = that.dateFormat(newTime, format);

        return newTime;
    };

    /** 
     * title: Tab选项卡插件(动态加载推荐用另一个最新的tabBox方法) 
     * @author kebai
     * @date 2015-12-03
     **/
    that.tabItem = function(options) {
        // 默认值
        var defaults = {
            tabMenu: ".tab-menu>li", //菜单选项
            tabBox: ".tab-box>div", //内容选项
            events: "click", //事件类型
            // dblclick: "dblclick",//双击事件
            active: "active", //事件焦点
            hover: "hover", //经过时的焦点
            curIndex: 1, //焦点索引定位
            callback: function() {} //回调函数
        };
        options = $.extend(defaults, options);
        var $tabMenu = $(options.tabMenu),
            $tabBox = $(options.tabBox);
        var events = options.events,
            active = options.active,
            dblclick=options.dblclick,
            hover = options.hover,
            curIndex = options.curIndex,
            callback = options.callback;
        if ($tabMenu.length == '0') {
            return;
        }


        // $tabMenu.off(dblclick).on(dblclick, function(event) {
        //     event.preventDefault(); //阻止默认事件
        //     curIndex = $(this).index();
        //     var nodeJson = {
        //         "parentId": curIndex+1,  //不同的配置页面id
        //     }
        //     var str = jQuery.param(nodeJson);//转换成字符串
        //
        //     window.open("../configure/configure.html?" + str);  //跳到PC大屏配置页面
        //     event.stopPropagation(); //阻止冒泡事件
        // });
        $tabMenu.off(events).on(events, function(event) {
            event.preventDefault(); //阻止默认事件
            curIndex = $(this).index();
            fnTab(curIndex);
            event.stopPropagation(); //阻止冒泡事件
        }).hover(function() {
            $(this).addClass(hover);
        }, function() {
            $(this).removeClass(hover);
        });
        var $obj;

        function fnTab(curIndex) {
            $obj = $tabMenu.eq(curIndex);
            $obj.addClass(active).siblings().removeClass(active);
            $tabBox.eq(curIndex).show().siblings().hide();
            callback(curIndex, $obj);
        }
        fnTab(curIndex);
    };

    /** 
     * title: 最新的Tab选项卡插件(事件委托的方法作用于动态加载tab标签时) 
     * @author kebai
     * @date 2016-10-25
     **/
    that.tabBox = function(options) {
        // 默认值
        var defaults = {
            tabParent: ".tab-wrap", //两者的父级标签(tabMenu,tabDiv)
            tabMenu: ".tab-li", //菜单选项
            tabDiv: ".tab-div", //内容选项
            events: "click", //事件类型
            active: "active", //事件焦点
            hover: "hover", //经过时的焦点
            curIndex: 0, //焦点索引定位
            callback: function() {} //回调函数
        };
        options = $.extend(defaults, options);
        var $obj, $tabParent = $(options.tabParent);
        var events = options.events,
            active = options.active,
            hover = options.hover,
            curIndex = options.curIndex,
            callback = options.callback;
        if ($tabParent.length == '0') {
            return;
        }

        function tabEvent($thisParent) {
            $thisParent.off(events).on(events, options.tabMenu, function(event) {
                event.preventDefault(); //阻止默认事件
                curIndex = $(this).index(options.tabMenu);
                fnTab($thisParent, curIndex);
                event.stopPropagation(); //阻止冒泡事件
            }).hover(function() {
                $(this).addClass(hover);
            }, function() {
                $(this).removeClass(hover);
            });
        }

        function fnTab(objParent, curIndex) {
            $obj = $(objParent).find(options.tabMenu).eq(curIndex);
            $obj.addClass(active).siblings().removeClass(active);
            $(objParent).find(options.tabDiv).hide();
            $(objParent).find(options.tabDiv).eq(curIndex).show();
            callback(curIndex, $obj);
        }

        $tabParent.each(function() {
            var $thisParent = $(this);
            tabEvent($thisParent);
            fnTab($thisParent, curIndex);
        });

    };


    /** 
     * title: 获取URL参数
     * name：key的名称
     * url：路径，默认本窗口
     * @author kebai
     * @date 2015-12-18
     **/
    that.getUrlParam = function(name, url) {
        url = url || window.location.href;
        url = url.toString();
        var newUrl, reg, result;
        newUrl = url.substr(url.indexOf('?') + 1, url.length);
        reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        result = newUrl.match(reg);
        if (result !== null) {
            result = decodeURI(decodeURI(result[2])); //防止二次编码情况
            return that.xss(result); //防止参数展示时xss攻击
        } else {
            return '';
        }
    };
    /** 
     * title: 获取URL指定位置后的文件名称
     * name：指定截取开始位置，默认/views/,必须以斜杠结尾
     * url：路径，默认本窗口
     * @author kebai
     * @date 2016-5-17
     **/
    that.getUrlName = function(name, url) {
        url = url || window.location.href;
        url = url.toString();
        name = name || '/views/';
        var newUrl, result = '';
        newUrl = url.substr(url.indexOf(name) + name.length, url.length);
        result = newUrl.split('/');
        if (result.length > 0) {
            result = result[0];
        }
        return result;
    };
    /** 
     * title: 替换url参数,没有则添加
     * key：key名称
     * value：替换的值 
     * url：替换的url，默认本窗口url   
     * @author kebai
     * @date 2015-12-23
     **/
    that.changeURL = function(key, value, url) {
        url = url || window.location.href;　　
        var par = key + "=" + value;
        if (url.indexOf("?" + key + "=") == -1 && url.indexOf("&" + key + "=") == -1) {
            var flag = url.indexOf("?") >= 0 ? "&" : "?";
            url = url + flag + par;
        } else {
            url = url.replace(eval('/(' + key + '=)([^&]*)/g'), par);　
        }
        return url;
    };

    /** 
     * title: 点击对象url链接跳转
     * objParent：父级对象
     * child：目标子级对象，默认为a元素
     * url：跳转url
     * dataValue：获取目标子级标签参数
     * events：事件类型
     * target：打开窗口类型，默认本窗口打开     
     * @author kebai
     * @date 2015-12-20
     **/
    that.openLink = function(objParent, child, url, dataValue, events, target) {
        var $objParent = $(objParent);
        child = child || 'a';
        dataValue = dataValue || '';
        events = events || 'click';
        target = target || '';
        if ($objParent.length == '0') {
            return 0;
        }
        if (dataValue) {
            if (url.substr(url.length - 1, url.length) != '=' && url.indexOf('?') >= 0) {
                url += '&'; //排除了？key=结尾的类型参数
            } else if (url.indexOf('?') < 0) {
                url += '?';
            }
        }
        $objParent.off(events).on(events, child, function(event) {
            event.preventDefault(); //阻止默认事件
            if (dataValue) {
                url += $(this).attr(dataValue);
            }
            if (!target) { //默认本窗口打开
                window.location.href = url;
            } else { //新窗口打开
                that.openNewWin(url);
            }
            event.stopPropagation(); //阻止冒泡事件
        });
    };

    /** 
     * title:导航链接焦点定位与点击跳转（根据文件名比配路径定位）  
     * obj:导航对象
     * clickFlag:点击是否跳转并传原有url参数(最好不要含有a标签,默认false不跳转)(clickFlag='noParam':为跳转不传原有url参数)
     * target:焦点对象，默认li
     * dataUrl:匹配路径，默认data-url,否则a的链接路径
     * active:焦点class，默认active
     * targetWin:窗口打开方式，默认当前窗口
     * @author kebai
     * @date 2016-11-4
     */
    that.linkActive = function(obj, clickFlag, target, dataUrl, active, targetWin) {
        obj = obj || '#menu-nav-link';
        clickFlag = clickFlag || false;
        target = target || 'li';
        dataUrl = dataUrl || 'data-url';
        active = active || 'active';
        targetWin = targetWin || '';
        var $obj = $(obj);
        if ($obj.length == '0') {
            return;
        }
        var url, winUrl = decodeURI(window.location.href),
            index = 0;
        $obj.find(target).each(function() {
            url = $(this).attr(dataUrl) || '';
            if (!url) {
                url = $(this).attr('href') || $(this).find('a').attr('href');
            }
            if (!url) {
                return;
            }
            index = url.indexOf('/');
            if (index != -1) { //只获取文件名和参数
                url = url.substr(index, url.length);
            }

            if (url && winUrl.indexOf(url) != -1) {
                $obj.find(target).removeClass(active);
                $(this).addClass(active);
            }
        });

        //点击跳转
        if (clickFlag) {
            var param = decodeURI(window.location.search),
                winTarget,
                urlNew, urlArr = [];
            $obj.find('a').on('click', function(event) {
                event.preventDefault();
            });
            $obj.on('click', target, function(event) {
                event.preventDefault();
                urlNew = $(this).attr(dataUrl) || $(this).attr('href') || $(this).find('a').attr('href') || '';

                if (!urlNew) {
                    return;
                }
                if (clickFlag != 'noParam') { //true跳转传参数
                    param = param.replace(/\?/g, "");
                    var flag = '&';
                    if (urlNew.indexOf('?') == -1) {
                        flag = '?';
                    } else {
                        urlArr = urlNew.split('?');
                        if (param.indexOf(urlArr[1]) != -1) { //已经存在参数则去掉
                            param += '&';
                            var reg = urlArr[1] + '&';
                            param = param.replace(reg, '').replace(/&*$/, ''); //去掉多加的&
                        }
                    }
                    if (param) {
                        urlNew += flag + param;
                    }
                }
                if (!targetWin) {
                    window.location.href = decodeURI(urlNew);
                } else {
                    that.openNewWin(urlNew); //新窗口
                }

            });
        }

    };

    /** 
     * title: 弹出新窗口中打开链接(会被拦截,不推荐使用)
     * url：打开路径
     * winName：窗口名称 
     * parameters：窗口参数,没有则默认全屏(有width=*,height=*宽高则默认居中)
     * @author kebai
     * @date 2016-1-12
     **/
    that.openWin = function(url, winName, parameters) {
        winName = winName || '_blank';
        parameters = parameters || '';
        if (url.length == '0') {
            return;
        }
        var winW, winH, top, left, param = 'toolbar=no, menubar=no, scrollbars=yes, resizable=no, location=no, status=no,';

        winW = that.getUrlParam('width', '?' + parameters.replace(',', '&'));
        winH = that.getUrlParam('height', '?' + parameters.replace(',', '&'));
        if (winW && winH) {
            //获得窗口的垂直位置
            top = (window.screen.availHeight - 30 - Number(winH.replace('px', ''))) / 2;
            //获得窗口的水平位置
            left = (window.screen.availWidth - 10 - Number(winW.replace('px', ''))) / 2;
            param += 'left=' + left + ',top=' + top + ',';
        }

        if (parameters) {
            param += parameters;
        } else {
            param += ',fullscreen=0';
        }

        if (/.*[\u4e00-\u9fa5]+.*$/.test(url)) {
            url = encodeURI(url); //含有中文则编码
        }

        window.open(url, winName, param);
    };

    /** 
     * title: 新窗口打开链接(不会被拦截，推荐使用)
     * url：打开路径
     * winName：窗口名称  
     * @author kebai
     * @date 2016-6-18
     **/
    that.openNewWin = function(url, winName) {
        winName = winName || '_blank';
        var element = document.createElement("a");
        document.body.appendChild(element);

        if (/.*[\u4e00-\u9fa5]+.*$/.test(url)) {
            url = encodeURI(url); //含有中文则编码
        }

        element.href = url;
        element.target = winName;
        element.click();
        document.body.removeChild(element);
    };

    /** 
     * title: post提交传参的方式打开新窗口(传敏感参数或者数据量很大时使用)
     * url：打开路径
     * json: json传参
     * winName：窗口名称  
     * @author kebai
     * @date 2017-02-07
     **/
    that.postOpenWin = function(url, json, winName) {
        winName = winName || '_blank';
        var $body = $(document.body),
            input;
        var form = $("<form method='post' accept-charset='utf-8' onsubmit=document.charset=\'utf-8\'  action=" + url + " target=" + winName + " id='post-open-win' style='display:none'></form>");
        form.appendTo($body);
        $form = $('#post-open-win');
        $.each(json, function(key, value) {
            input = $("<input type='hidden'>");
            input.attr({
                "name": key
            }).val(value);
            $form.append(input);
        });
        $form.get(0).submit();
        $form.remove();
    };


    /** 
     * title: 是否确定关闭当前页面
     * btn:关闭按钮，默认close-win-btn
     * @author kebai
     * @date 2016-6-21
     **/
    that.closeWin = function(btn, title, content) {
        btn = btn || '.close-win-btn';
        title = title || "关闭窗口";
        content = content || "您确定要关闭本窗口吗？";
        $(btn).off('click').on('click', function() {

            if (dialog) {
                that.dialogShow(title, content, function() {
                    that.windowClose();
                });
            } else {
                if (confirm(content)) {
                    that.windowClose();
                }
            }
        });
    };

    /** 
     * title: 直接关闭窗口
     * setTime:延时时间
     * @author kebai
     * @date 2017-9-18
     **/
    that.windowClose = function(setTime) {
        setTime = setTime || '';
        var setT = null;
        if (setTime) {
            setT = setTimeout(function() {
                fnClose();
            }, setTime);
        } else {
            fnClose();
        }

        function fnClose() {
            window.opener = null;
            window.open('about:blank', '_self');
            window.close();
            window.location.href = "about:blank";
        }
    };


    /** 
     * title: 点击导航菜单返回相应的参数
     * objParent：父级对象
     * child：目标子级对象，默认为a元素
     * callback: 回调函数
     * dataValue：获取目标子级标签参数，默认'data-value'
     * events：事件类型，默认点击事件 
     * @author kebai
     * @date 2015-12-20
     **/
    that.navMenu = function(objParent, child, callback, dataValue, active, events) {
        var $objParent = $(objParent);
        child = child || 'a';
        dataValue = dataValue || 'data-value';
        active = active || 'active';
        events = events || 'click';
        callback = callback || null;
        if ($objParent.length == '0') {
            return 0;
        }
        var value = null;
        $objParent.off(events).on(events, child, function(event) {
            event.preventDefault(); //阻止默认事件 
            var $this = $(this);
            $this.addClass(active).siblings().removeClass(active);
            value = $this.attr(dataValue) || '';
            callback && callback($objParent, value, $this.index());
            event.stopPropagation(); //阻止冒泡事件
        });
    };

    /** 
     * title: 多层级树形结构展开/折叠菜单（显示/隐藏）
     * objParent：父级对象
     * child： 子级对象，默认为.unit
     * target: 目标显示对象,只能是紧跟child后面，默认ul
     * flag:  是否只能显示其中一个其它全部隐藏，默认false，可以显示多个(只对点击事件有效)
     * events：事件类型，默认click点击事件,若为hover鼠标经过事件则只展开显示一个
     * active：事件焦点类active
     * onActive:经过层级展开的焦点类on-active
     * callback: 回调函数
     * @author kebai
     * @date 2017-10-13
     **/
    that.foldMenu = function(objParent, child, target, callback, flag, events, active, onActive) {
        var $objParent = $(objParent);
        child = child || '.unit';
        target = target || 'ul';
        $child = $objParent.find(child);
        $target = $objParent.find(target);
        flag = flag || false;
        active = active || 'active';
        onActive = onActive || 'on-active';
        events = events || 'click';
        callback = callback || null;
        if ($objParent.length == '0') {
            return 0;
        }
        var $this, $thisTarget, setT = null;
        if (events == 'hover') { //经过展开，只显示一个 
            fnHover();
        } else if (events == 'click') {
            fnCick();
        }

        function fnHover() {
            $child.hover(function() {
                $this = $(this);
                $this.addClass(active);
                $this.parents(target).siblings(child).addClass(onActive); //经过的父级添加类
                $this.parents(target).addClass('on-target');
                $objParent.find(target).not('.on-target').hide(); //隐藏其它目标对象
                $thisTarget = $this.next(target);
                if ($thisTarget.length > 0) {
                    $thisTarget.show();
                    $this.addClass(onActive);
                }
                callback && callback($objParent, $this);
            }, function() {
                $this = $(this);
                $thisTarget = $this.next(target);
                $this.removeClass(onActive + ' ' + active);
                $this.parents(target).removeClass('on-target');
                $this.parents(target).siblings(child).removeClass(onActive);
                clearTimeout(setT);
                setT = setTimeout(function() {
                    if ($objParent.find('.' + active).length == 0) {
                        $objParent.find(target).hide(); //完全离开全部隐藏
                    }
                }, 30);
            });
        }

        function fnCick() {
            $objParent.off(events).on(events, child, function(event) {
                event.preventDefault(); //阻止默认事件 
                $this = $(this);
                $thisTarget = $this.next(target);

                $objParent.find('.' + active).removeClass(active); //去掉所有焦点
                $this.addClass(active);

                if (!flag) { //显示多项 
                    if ($thisTarget.is(":visible")) {
                        $thisTarget.hide();
                        $this.removeClass(onActive + ' ' + active);
                    } else if ($thisTarget.length > 0) {
                        $thisTarget.show();
                        $this.addClass(onActive);
                    }
                } else { //只显示一项   
                    if ($this.hasClass(onActive)) {
                        $thisTarget.hide();
                        $this.removeClass(onActive + ' ' + active);
                    } else {
                        $objParent.find('.' + onActive).removeClass(onActive);
                        $this.parents(target).siblings(child).addClass(onActive); //经过的父级添加类

                        $objParent.find('.on-target').removeClass('on-target');
                        $this.parents(target).addClass('on-target');
                        $objParent.find(target).not('.on-target').hide(); //隐藏其它目标对象

                        if ($thisTarget.length > 0) {
                            $thisTarget.show();
                            $this.addClass(onActive);
                        }
                    }
                }
                callback && callback($objParent, $this);
                event.stopPropagation(); //阻止冒泡事件
            });
        }
    };


    /** 
     * title: 进度条动画
     * objParent：父级对象
     * child：目标子级对象，默认为as-pg-bar类元素 
     * dataValue：获取目标子级标签参数,默认为data-value
     * Time：动画时间，默认600毫秒 
     * @author kebai
     * @date 2015-12-22
     **/
    that.progress = function(objParent, child, dataValue, Time) {
        var $objParent = $(objParent);
        child = child || '.as-pg-bar';
        dataValue = dataValue || 'data-value';
        Time = Time || 600;
        if ($objParent.length == '0') {
            return 0;
        }
        var $this, width;
        $objParent.find(child).each(function() {
            $this = $(this);
            width = $this.attr(dataValue) + "%";
            $this.stop().animate({
                width: width
            }, Time);
        });

    };

    /** 
     * title: 滚动指定位置
     * obj：点击对象
     * target：目标对象,滚动到的位置，默认为0 
     * events：事件类型，默认为点击触发
     * Time：动画时间，默认200毫秒 
     * @author kebai
     * @date 2015-12-22
     **/
    that.scrollTop = function(obj, target, events, Time) {
        var $obj = $(obj);
        target = target || '';
        events = events || 'click';
        Time = Time || 200;
        if ($obj.length == '0') {
            return 0;
        }
        var scrollValue;
        var top = $(target).length != 0 ? $(target).offset().top : 0;
        $(window).scroll(function() {
            scrollValue = $(window).scrollTop();
            scrollValue > 100 ? $obj.fadeIn() : $obj.fadeOut();
        });
        $obj.off(events).on(events, function() {
            $("html,body").stop().animate({
                scrollTop: top
            }, Time);
        });
    };

    /** 
     * title: 动态加载js(推荐用$.getScript)、css文件
     * file：文件名
     * callback：回调函数，默认为空 
     * target: 目标位置,默认头部
     * type: 位置类型
     * @author kebai
     * @date 2015-12-22
     **/
    that.loadFile = function(file, callback, target, type) {
        if (file.length == '0') {
            return;
        }
        callback = callback || null;
        target = target || 'head';
        var flag = true;
        if (file.indexOf('.js') > 0) { //js文件
            tag = document.createElement('script');
            tag.type = 'text/javascript';
            flag = true;
        } else if (file.indexOf('.css') > 0) { //css文件
            tag = document.createElement('link');
            tag.rel = "stylesheet";
            tag.type = 'text/css';
            flag = false;
        }
        tag.onload = tag.onreadystatechange = function() {
            if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") {
                callback && callback();
                // Handle memory leak in IE 
                tag.onload = tag.onreadystatechange = null;
            }
        };
        if (flag) {
            $(target).find("script[src='" + file + "']").remove(); //防止重复添加
            tag.src = file;
        } else {
            $(target).find("link[href='" + file + "']").remove(); //防止重复添加
            tag.href = file;
        }
        switch (type) {
            case "after":
                $(target).after(tag);
                break;
            case "before":
                $(target).before(tag);
                break;
            case "prepend":
                $(target).prepend(tag);
                break;
            default:
                $(target).append(tag);
                break;
        }
    };

    /** 
     * title:指定div位置局部加载url文件(ajax,iframe)
     * @author kebai
     * @date 2017-11-03
     **/
    that.loadUrl = function(options) {
        var defaults = {
            url: null, //文件路径(若为空则获取路径hash,页面刷新时加载)
            param: {}, //json参数
            obj: '.as-load-cont', //加载文件显示位置（iframe方式时obj需要设置高度）
            type: 'ajax', //加载方式(ajax,iframe),默认ajax
            callback: function() {} //回调函数
        };
        var opts = $.extend(defaults, options),
            callback = opts.callback;
        var $obj = $(opts.obj),
            iframe, $iframe, srcUrl = opts.url,
            type = opts.type,
            validFlag = 0;

        if (!srcUrl) { //页面刷新时
            if (window.srcUrlonLoad) { return; }
            validFlag = 1;
            var hash = window.location.hash,
                len = hash.length,
                index = hash.indexOf('#src='),
                index2 = hash.indexOf('#ifsrc=');
            type = '';
            if (index != -1) {
                type = 'ajax';
                srcUrl = hash.substring(index + 5, len);
            } else if (index2 != -1) {
                type = 'iframe';
                srcUrl = hash.substring(index2 + 7, len);
            } else {
                return;
            }
            window.srcUrlonLoad = true; //防止循环加载
        }
        $.ajaxSetup({
            cache: false //不设置缓存
        });
        if (type == 'ajax') {
            $obj.html('');
            var saveSeajs = window.seajs || {};
            if (window.seajs) { //置空才能重新加载seajs模块(最好是去掉define,单独标签引入模块js)
                window.seajs = null;
            }

            $.ajaxSetup({
                cache: true //设置缓存
            });
            $obj.load(srcUrl, opts.param, function(data) {
                if (!window.seajs) { //若没加载seajs则回复其值
                    window.seajs = saveSeajs;
                }
                callback($obj, data, 'ajax');
                that.openNewWin('#src=' + srcUrl, '_self'); //修改hash值
            });
        } else if (type == 'iframe') {
            var param = $.param(opts.param),
                flag = srcUrl.indexOf('?') == -1 ? '?' : '&';
            if (param) { srcUrl += flag + param; }
            iframe = '<iframe id="iframe-src" src="" width="100%" height="100%" frameborder="no" border="0" marginwidth="0" marginheight="0" scrolling="yes"></iframe>';
            $iframe = $obj.find('#iframe-src');
            if ($iframe.length == '0') {
                $obj.html(iframe);
                $iframe = $obj.find('#iframe-src');
            }

            function fnSrc() {
                $iframe.attr('src', srcUrl);
                $iframe.on('load', function() {
                    callback($obj, $iframe, 'iframe');
                    that.openNewWin('#ifsrc=' + srcUrl, '_self'); //修改hash值
                });
            }

            if (!validFlag) {
                fnSrc();
            } else {
                //验证路径是否有效
                that.validUrl(srcUrl, validcallback);

                function validcallback(response, status, flag) {
                    if (flag == 1) {
                        fnSrc();
                    }
                }
            }
        }
    };


    /** 
     * title: 清楚html标签,返回纯文本
     * str：字符串 
     * @author kebai
     * @date 2015-12-29
     **/
    that.removeHtml = function(str) {
        if (str.length == '0') {
            return '';
        }
        var reg = /<("[^"]*"|'[^']*'|[^'">])*>/gi;
        return str.replace(reg, '');
    };


    /** 
     * title: 检查对象是否存在
     * obj：检查对象 
     * callback：回调函数，默认为空 
     * @author kebai
     * @date 2015-12-29
     **/
    that.hasObj = function(obj, callback) {
        var $obj = $(obj);
        callback = callback || null;
        if ($obj.length == '0') {
            return null;
        }
        callback && callback($obj);
        return $obj;
    };

    /** 
     * title: 获取表单所有字段，包括隐藏input，json形式返回(推荐用formJson的方法)
     * obj：表单对象(若有placeholder，则用formJson的方法) 
     * flag:值是否要encodeURI编码，默认false不编码，(表单post请求不要编码，get请求则需要编码)
     * valStrType:值为字符串类型,数组值转成字符串，默认true(转成字符串,若获取所有表单数据后整体要转成字符串，则这里须设置为false)
     * comma:默认逗号分隔
     * xssFlag:默认true,true对html标签实体转义
     * @author kebai
     * @date 2015-12-31
     **/
    that.serializeJson = function(obj, flag, valStrType, comma, xssFlag) {
        var $obj = $(obj);
        if ($obj.length == '0') {
            return '';
        }
        flag = flag || false;
        valStrType = valStrType == false ? false : true;
        comma = comma || ',';
        xssFlag = xssFlag == false ? false : true;
        var value, valueArr, json = {},
            arr = $obj.serializeArray(); //对没有选中的复选框、单选框、多选下拉框不会返回空值因此(推荐用formJson的方法)
        $.each(arr, function() {
            value = $.trim(this.value) || '';
            if (json[this.name] !== undefined) {
                if (!json[this.name].push) {
                    json[this.name] = [json[this.name]];
                }
                json[this.name].push(value);
            } else {
                json[this.name] = value;
            }
        });
        for (var i in json) {
            value = json[i];
            if (that.isArray(value)) {
                //数组转成字符串
                if (valStrType) {
                    value = value.join(comma);
                }
            }
            if (flag && value) {
                value = encodeURI(value);
            }
            if (xssFlag) { //过滤实体转义
                value = that.xss(value);
            }
            json[i] = value;
        }
        return json;
    };

    /** 
     * title: 获取指定div中所有表单字段，包括隐藏input，json形式返回
     * obj：div对象
     * flag:值是否要encodeURI编码，默认false不编码，(表单post请求不要编码，get请求则需要编码)
     * valStrType:值为字符串类型,数组值转成字符串，默认true(转成字符串,若获取所有表单数据后整体要转成字符串，则这里须设置为false)
     * comma:默认逗号分隔
     * xssFlag:默认true,true对html标签实体转义
     * @author kebai
     * @date 2017-12-26
     **/
    that.formJson = function(obj, flag, valStrType, comma, xssFlag) {
        var $obj = $(obj);
        if ($obj.length == '0') {
            return;
        }
        flag = flag || false;
        valStrType = valStrType == false ? false : true;
        comma = comma || ',';
        xssFlag = xssFlag == false ? false : true;
        var json = {},
            $this, name, value, valueArr;
        //文本、文本域,非按钮类型
        $obj.find("input:not('input[type=radio],input[type=checkbox],input[type=button],input[type=submit]'),textarea").each(function() {
            $this = $(this);
            name = $this.attr('name');
            value = $this.val();
            value = $.trim(value); //去空格
            if (!value && value != 0) {
                value = '';
            }
            if (value == $this.attr('placeholder')) {
                value = '';
            }
            if (flag && value) { //中文编码
                value = encodeURI(value);
            }
            if (xssFlag) { //过滤实体转义
                value = that.xss(value);
            }
            if (name) {
                json[name] = value;
            }
        });
        //下拉
        $obj.find("select").each(function() {
            $this = $(this);
            name = $this.attr('name');
            value = $this.val();
            value = $.trim(value); //去空格
            if (!value && value != 0) {
                value = '';
            }
            if (that.isArray(value)) {
                //多选
                if (valStrType) {
                    value = that.toJson(value, false);
                }
                if (flag && value) { //中文编码
                    value = encodeURI(value);
                }
                if (xssFlag) { //过滤实体转义
                    value = that.xss(value);
                }
                if (name) {
                    json[name] = value;
                }
            } else {
                //单选
                if (flag && value) { //中文编码
                    value = encodeURI(value);
                }
                if (xssFlag) { //过滤实体转义
                    value = that.xss(value);
                }
                if (name) {
                    json[name] = value;
                }
            }
        });
        //单选框
        $obj.find("input[type=radio]").each(function() {
            $this = $(this);
            name = $this.attr('name');
            value = $this.val();
            value = $.trim(value); //去空格
            if (!json[name]) {
                json[name] = ''; //设置空值
            }
            if (!value && value != 0) {
                value = '';
            }
            if (flag && value) {
                value = encodeURI(value);
            }
            if (xssFlag) { //过滤实体转义
                value = that.xss(value);
            }
            if (name && $this.is(':checked')) {
                json[name] = value;
            }
        });
        //复选框
        var checkName, checkArr = [];
        $obj.find("input[type=checkbox]").each(function() {
            $this = $(this);
            checkName = $this.attr('name');
            value = $this.val();
            value = $.trim(value); //去空格
            if (!json[checkName]) {
                json[checkName] = ''; //设置空值
                checkArr = [];
            }
            if (checkName && $this.is(':checked')) {
                checkArr.push(value);
                valueArr = checkArr;
                if (valStrType) {
                    valueArr = checkArr.join(comma);
                }
                if (flag && valueArr) {
                    valueArr = encodeURI(valueArr);
                }
                if (xssFlag) { //过滤实体转义
                    value = that.xss(value);
                }
                json[checkName] = valueArr;
            }
        });
        return json;
    };

    /** 
     * title: 清除所有表单数据(包括默认值),跟表单重置有所区别
     * obj: 表单对象
     * @author kebai
     * @date 2017-5-25
     **/
    that.clearForm = function(obj) {
        var $obj = $(obj);
        if ($obj.length == '0') {
            return;
        }
        var type, tag;
        $obj.find("input:not(':disabled'),textarea:not(':disabled'),select:not(':disabled')").each(function() {
            type = this.type;
            tag = this.tagName.toLowerCase();
            if (tag == 'select') {
                if (this.multiple) {
                    this.selectedIndex = -1;
                } else {
                    this.selectedIndex = 0;
                }
            } else if (type == 'checkbox' || type == 'radio') {
                this.checked = false;
            } else if (type != 'button' && type != 'submit') {
                this.value = "";
            }
        });
    };


    /** 
     * title: 获取表格表单数据
     * parentObj:父级对象（表格）
     * obj:列表对象，默认tr
     * callback:回调函数(多个表格时)
     * @author kebai
     * @date 2017-10-11
     */
    that.getDataTable = function(parentObj, obj, callback) {
        obj = obj || 'tr';
        var $parentObj = $(parentObj),
            result = [],
            json = {},
            $this, $parent;
        if ($parentObj.length == '0') { return; }
        $parentObj.each(function(index) {
            result = [];
            $parent = $(this);
            $parent.find(obj).each(function() {
                $this = $(this);
                json = that.formJson($this);
                result.push(json);
            });
            callback && callback(result, $parent, index);
        });
        return result; //父级对象唯一性
    };


    /** 
     * title: 下拉选项改变时动态创建input标签赋值其相应的文本值
     * obj: 下拉对象，多个逗号隔开
     * name: 文本字段名，多个逗号隔开
     * @author kebai
     * @date 2015-7-15
     **/
    that.selectChange = function(obj, name) {
        var objArr = obj.split(','),
            nameArr = name.split(',');
        $.each(objArr, function(i, value) {
            fn($(value), nameArr[i]);
        });

        function fn($obj, nameNew) {
            var $newObj = $obj.next('.' + nameNew);
            if ($newObj.length == '0') {
                $obj.after('<input type="hidden" name="' + nameNew + '" placeholder="" class="' + nameNew + '">');
            }
            $newObj = $obj.next('.' + nameNew);
            $newObj.val($obj.find(':selected').text());
            $obj.off('change').on('change', function() {
                $newObj.val($(this).find(':selected').text());
            });

        }

    };

    /** 
     * title: 单击按钮显示对象，单击对象外部任意处隐藏对象
     * btn：触发按钮
     * obj：对象 
     * events:触发事件，默认单击事件
     * closebtn:关闭按钮,隐藏对象,默认close-btn
     * callback：回调函数，默认为空  
     * active:添加焦点,默认active
     * blankFlag:false//单击对象外部任意处隐藏对象
     * @author kebai
     * @date 2016-1-1
     **/
    that.objShow = function(btn, obj, events, closebtn, callback, active, blankFlag) {
        var $obj = $(obj),
            $btn = $(btn);
        if ($obj.length == '0') {
            return '';
        }

        events = events || 'click';
        callback = callback || null;
        active = active || "active";
        closebtn = closebtn || ".close-btn";
        $btn.off(events).on(events, function(event) {
            event.preventDefault(); //阻止默认事件  
            if ($obj.is(':visible')) {
                $obj.hide();
                $btn.removeClass(active);
            } else {
                $obj.show();
                $btn.addClass(active);
            }
            callback && callback($obj);
            event.stopPropagation(); //阻止冒泡事件
        });
        $obj.off('click').on('click', function(event) {
            event.stopPropagation(); //阻止冒泡事件
        });
        $(document).not($obj).on('click', function() {
            if (!blankFlag) {
                if ($obj.is(':visible')) {
                    $obj.hide();
                    $btn.removeClass(active);
                }
            }

        });
        $(closebtn).off('click').on('click', function() {
            $obj.hide();
            $btn.removeClass(active);
        });

        return $obj;
    };

    /** 
     * title: 鼠标经过显示层，离开隐藏层  
     * objParent：父级对象（经过对象和目标对象的父级）
     * obj：经过对象
     * target：目标显示对象 
     * sameLevel: 只对obj同级元素有效，默认false，不是同级也有效
     * callback：回调函数，默认为空
     * flag: 离开是否隐藏，默认true隐藏
     * closebtn:关闭按钮,隐藏目标对象,默认close-btn 
     * active:添加焦点,默认active 
     * @author kebai
     * @date 2016-5-19
     **/
    that.hoverShow = function(objParent, obj, target, sameLevel, callback, flag, closebtn, active) {
        var $objParent = $(objParent),
            $obj = $objParent.find(obj),
            $target = $objParent.find(target);
        if ($objParent.length == '0' || $obj.length == '0') {
            return;
        }
        sameLevel = sameLevel || false;
        callback = callback || null;
        flag = flag == false ? false : true;
        closebtn = closebtn || ".close-btn";
        active = active || "active";
        var outFlag, $this, setT = null;
        $obj.hover(function() {
            $this = $(this);
            $this.addClass(active);
            $target.hide();
            if (sameLevel) {
                $this.nextAll(target).show();
            } else {
                $target.show();
            }

            callback && callback($obj, $target, $this);
            outFlag = false;
        }, function() {
            outFlag = true;
            $target.hover(function() {
                outFlag = false;
            }, function() {
                outFlag = true;
                fnHide();
            });
            fnHide();
        });

        function fnHide() {
            clearTimeout(setT);
            setT = setTimeout(function() {
                if (flag && outFlag) {
                    $target.hide();
                    $obj.removeClass(active);
                }
            }, 80);
        }
        //关闭隐藏
        $objParent.find(closebtn).off('click').on('click', function() {
            $target.hide();
            $obj.removeClass(active);
            callback && callback($obj, $target, $(this));
        });
    };

    /** 
     * title: 鼠标经过提示文字层 
     * obj：对象 
     * id: 提示框id
     * flag: 是否跟随移动，默认true，跟随移动
     * dataValue:获取显示文字值字段，默认data-value
     * zIndex：提示层层次,默认100
     * @author kebai
     * @date 2016-1-26
     **/
    that.toolTip = function(obj, id, flag, dataValue, zIndex, x, y) {
        var $obj = $(obj);
        id = id || "#tooltip";
        flag = flag == false ? false : true;
        dataValue = dataValue || "data-value";
        zIndex = zIndex || 100;
        if ($obj.length == '0') {
            return;
        }
        x = x || 10, y = y || 10; //偏移量
        var $tooltip, tooltip, msg;
        $obj.mouseover(function(e) {
            msg = $(this).attr(dataValue);
            if (!$tooltip) {
                tooltip = "<div class='tooltip' id=" + id.replace('#', '') + "></div>";
                $("body").append(tooltip);
                $tooltip = $(id);
            }
            $tooltip.css({
                "top": (e.pageY + y) + "px",
                "left": (e.pageX + x) + "px",
                "position": "absolute",
                "zIndex": zIndex
            }).html(msg).show();
        }).mouseout(function(e) {
            $tooltip.hide();
        }).mousemove(function(e) {
            if (flag) {
                $tooltip.css({
                    "top": (e.pageY + y) + "px",
                    "left": (e.pageX + x) + "px"
                });
            }
        });
    };

    /** 
     * title: 鼠标经过提示文字
     * obj:标签对象
     * dataTitle:获取显示文字值字段，默认"data-title" 
     * @author kebai
     * @date 2016-1-26
     **/
    that.hoverTips = function(obj, dataTitle, width) {
        var $tag, $this, $tooltips, $text, title, left, top;
        dataTitle = dataTitle || "data-title";
        obj = obj || ".hover-tips-title,a[title],span[title],p[title]";
        var $obj = $(obj);
        if ($obj.length == '0') {
            return;
        }
        if ($("#as-tooltips").length == '0') {
            var tips = '<div id="as-tooltips" class="as-tooltips" style="display:none;width:auto;position: absolute; z-index: 1000;"><span class="as-tips-title"></span></div>';
            $("body").append(tips);
        }
        $tooltips = $("#as-tooltips");
        $text = $tooltips.find('.as-tips-title');
        $obj.each(function() {
            $tag = $(this);
            $tag.hover(function(e) {
                $this = $(this);
                title = $this.attr('title') || '';
                if (title) {
                    $this.attr('title', '');
                    $this.attr(dataTitle, title);
                } else {
                    title = $this.attr(dataTitle);
                }
                $text.html(title);
                if (width) {
                    $text.css({
                        "maxWidth": width
                    });
                }
                //只有显示了才有宽高
                $tooltips.show();
                left = e.pageX - $text.width() / 2 - 10;
                top = e.pageY - $text.height() - 40;
                $tooltips.hide();
                $tooltips.css({
                    "left": left,
                    "top": top
                }).show();
            }, function() {
                $tooltips.hide();
            });

        });
        //点击隐藏 
        $(document).not($tooltips).on('click', function(e) {
            $tooltips.hide();
        });
    };


    /** 
     * title: 下拉选择框
     * objParent:父级对象 
     * obj：点击对象  
     * target:下拉对象,默认文本对象的下一个同辈标签
     * callback: 回调函数
     * objText: 显示文本对象 
     * unit: 列表元素对象
     * events:触发事件，默认单击事件
     * dataValue: 获取显示文字值字段，默认data-value 
     * active:添加焦点,默认active 
     * @author kebai
     * @date 2016-3-25
     **/
    that.selectBox = function(objParent, obj, target, callback, objText, unit, events, dataValue, active) {
        objParent = objParent || '.as-select-box';
        obj = obj || '.as-sel-item';
        var $objParent = $(objParent);
        if ($objParent.length == '0') {
            return;
        }
        target = target || '.as-sel-ul';
        callback = callback || null;
        objText = objText || ".sel-text";
        unit = unit || ".as-u";
        events = events || 'click';
        dataValue = dataValue || "data-value";
        active = active || 'active';

        var value, text, $this;

        function fnBox($this) {
            $this.addClass(active);
            value = $this.attr(dataValue);
            text = $this.text();
            $objParent = $this.parents(objParent);
            $obj = $objParent.find(obj);
            $obj.find(objText).text(text);
            $objParent.find('input').attr("data-sel-value", value); //找到文本框并赋值
            $objParent.find('.sel-input').val(value); //找到隐藏文本框并赋值
            $objParent.attr("data-sel-value", value);
            callback && callback($obj, $this, value);
        }
        var flag = 0;
        $objParent.each(function() {
            var $parent = $(this);
            var $obj = $parent.find(obj);
            var $target = $parent.find(target);

            that.objShow($obj, $target, events, null, function() {
                if ($target.is(":visible")) {
                    flag = 1;
                } else {
                    flag = 0;
                }
                $(target).hide();
                if (flag) {
                    $target.show();
                }
            }); //显示和隐藏  

            $target.off(events).on(events, unit, function(event) {
                $target.find(unit).removeClass(active);
                $this = $(this);
                fnBox($this);
            });

            //初始化默认取第一个值 
            var $active, $firstObj;
            $active = $target.find('.' + active);
            $firstObj = $active.length > 0 ? $active : $target.find(unit).eq(0);
            fnBox($firstObj);
        });


    };

    /** 
     * title: 单选框
     * objParent:父级对象 
     * obj：点击对象   
     * callback: 回调函数 
     * events:触发事件，默认单击事件 
     * active:添加焦点,默认active 
     * @author kebai
     * @date 2016-6-4
     **/
    that.radioBox = function(objParent, obj, callback, active, events) {
        objParent = objParent || '.as-radio-box';
        obj = obj || '.as-rad-label';
        var $objParent = $(objParent),
            $active, $thisParent;
        if ($objParent.length == '0') {
            return;
        }
        active = active || 'active';
        events = events || 'click';
        callback = callback || null;
        $objParent.each(function() {
            $thisParent = $(this);

            function fn() {
                $active = $thisParent.find('.' + active);
                $thisParent.find('input').prop('checked', false);
                $active.find('input').prop('checked', true);
                callback && callback($active, $active.index());
            }
            fn();
            that.objAddClass(objParent, obj, function() {
                fn();
            }, 1, false, true, events, active);

        });

    };

    /** 
     * title: 复选框
     * objParent:父级对象 
     * obj：点击对象   
     * callback: 回调函数 
     * events:触发事件，默认单击事件 
     * active:添加焦点,默认active 
     * @author kebai
     * @date 2016-6-16
     **/
    that.checkBox = function(objParent, obj, callback, active, events) {
        objParent = objParent || '.as-check-box';
        obj = obj || '.as-che-label';
        var $objParent = $(objParent),
            $active, $thisParent;
        if ($objParent.length == '0') {
            return;
        }
        active = active || 'active';
        events = events || 'click';
        callback = callback || null;
        $objParent.each(function() {
            $thisParent = $(this);

            function fn() {
                $active = $thisParent.find('.' + active);
                $thisParent.find('input').prop('checked', false);
                $active.find('input').prop('checked', true);
                callback && callback($active, $active.index());
            }
            fn();
            that.objAddClass(objParent, obj, function() {
                fn();
            }, 2, false, true, events, active);

        });
    };

    /** 
     * title: file文件模拟框
     * objParent:父级对象 
     * obj：点击对象   
     * callback: 回调函数 
     * @author kebai
     * @date 2016-6-16
     **/
    that.fileBox = function(objParent, obj, file, text, callback) {
        objParent = objParent || '.as-file-box';
        obj = obj || '.as-file-btn';
        file = file || '.as-input';
        text = text || '.as-file-text';
        var $objParent = $(objParent);
        if ($objParent.length == '0') {
            return;
        }
        var $this, $file;
        $objParent.each(function(i) {
            $this = $(this), $file = $this.find(file);
            $this.find(obj).off('click').on('click', function() {
                $file.trigger('click');
            });

            $file.off('change').on('change', function() {
                var val = $(this).val();
                if (val) {
                    $this.find(text).text(val);
                }
            });
        });

    };
    /** 
     * title: 根据索引定位焦点，添加class，或者焦点事件
     * objParent:父级对象   
     * obj:对象
     * index:定位对象索引,默认0
     * callback:回调函数 
     * sameLevel: 只对obj同级元素有效，默认false，不是同级也有效
     * events:事件触发，默认点击事件
     * active:添加焦点类，默认class为'active'  
     * flag:再次点击时去掉焦点，默认false,不去掉
     * @author kebai
     * @date 2016-3-28
     **/
    that.objActive = function(objParent, obj, index, callback, sameLevel, events, active, flag) {
        var $objParent = $(objParent),
            $findObj = $objParent.find(obj);
        if ($objParent.length == '0') {
            return;
        }
        index = index || 0;
        callback = callback || null;
        sameLevel = sameLevel || false;
        events = events || 'click';
        active = active || 'active';
        flag = flag || false;

        if (typeof index == 'number') {
            $findObj.removeClass(active).eq(index).addClass(active);
            callback && callback($findObj.eq(index), index);
        }
        var $this, num;
        $objParent.off(events).on(events, obj, function() {
            $this = $(this);
            num = $this.index();
            if (!$this.hasClass(active)) {
                if (!sameLevel) { //非同级元素
                    $findObj.removeClass(active);
                    $this.addClass(active);
                } else { //同级元素
                    $this.addClass(active).siblings().removeClass(active);
                }
            } else if (flag) {
                $this.removeClass(active);
                num = -1;
            }
            callback && callback($this, num);

        });


    };

    /** 
     * title: 事件添加class，单个或多个选中状态
     * objParent:父级对象   
     * obj:对象 
     * callback:回调函数  
     * type: 单个(1)或者多个(>1)，默认单个(1) 
     * flag:再次点击时去掉焦点，默认false,不去掉,只对单个有效,多个已默认去焦点
     * sameLevel: 只对obj同级元素有效，默认false，不是同级也有效
     * events:事件触发，默认点击事件
     * active:添加焦点类，默认class为'active'
     * @author kebai
     * @date 2016-5-16
     **/
    that.objAddClass = function(objParent, obj, callback, type, flag, sameLevel, events, active) {
        var $objParent = $(objParent),
            $findObj = $objParent.find(obj);
        if ($objParent.length == '0') {
            return;
        }
        callback = callback || null;
        type = type > 1 ? type : 1;
        flag = flag || false;
        sameLevel = sameLevel || false;
        events = events || 'click';
        active = active || 'active';


        var $this, $thisParent;
        $objParent.off(events).on(events, obj, function() {
            $this = $(this);
            var index = $this.index(); //为-1时为去掉焦点
            if (type == 1) {
                if (!sameLevel) { //非同级元素
                    $findObj.removeClass(active);
                    $this.addClass(active);
                } else { //同级元素
                    $this.addClass(active).siblings().removeClass(active);
                }
                if ($this.hasClass(active) && flag) {
                    $this.removeClass(active);
                    index = -1;
                }

            } else { //选中多个 

                if (!$this.hasClass(active)) {
                    $this.addClass(active);

                    if ($this.attr('data-all') == 'all') {
                        //全选
                        $this.parents(objParent).find(obj).addClass(active);
                        $this.parents(objParent).find('input').prop('checked', true);
                    }
                } else {
                    $this.removeClass(active);
                    index = -1;

                    if ($this.attr('data-all') == 'all') {
                        //全不选
                        $this.parents(objParent).find(obj).removeClass(active);
                        $this.parents(objParent).find('input').prop('checked', false);
                    }
                }

            }

            $thisParent = $this.parents(objParent);
            callback && callback($this, index, type, $thisParent);
            //去掉input冒泡的影响
            $this.find('input').off(events).on(events, function(event) {
                event.stopPropagation();
            });

        });

    };

    /** 
     * title:两个类的相互替换 
     * obj:对象 
     * className1,className2,类名 
     * @author kebai
     * @date 2016-5-19
     **/
    that.changeClass = function(obj, className1, className2) {
        var $obj = $(obj);
        className1 = className1.replace(/#|\./g, '');
        className2 = className2.replace(/#|\./g, '');
        if ($obj.hasClass(className1)) {
            $obj.removeClass(className1).addClass(className2);
        } else {
            $obj.removeClass(className2).addClass(className1);
        }
    };

    /** 
     * title:获取json数组并返回传参url
     * array:数据源，json数组[{"urlName":"list","name":[1,2],"value":[1,2]}]
     * index:数组索引,默认为0
     * json:返回是否是json对象，默认为url形式
     * name:键名，value：键值，urlName：路径名
     * @author kebai
     * @date 2016-4-22
     **/

    that.getArrayUrl = function(array, index, json, name, value, urlName) {
        var result = '',
            arr = [],
            data = {};
        index = index || 0;
        json = json || false;
        name = name || 'name';
        value = value || 'value';
        urlName = urlName || 'urlName';
        array = array[index];
        var length = array[name].length;
        if (length == 0) {
            return;
        }
        if (json) { //返回json对象
            for (var i = 0; i < length; i++) {
                data[array[name][i]] = encodeURI(array[value][i]);
            }
            result = data;
        } else { //返回url参数
            for (var i = 0; i < length; i++) {
                arr.push(array[name][i] + '=' + encodeURI(array[value][i]));
            }
            result = arr.join('&');
            if (array[urlName]) {
                var url = getPath(array[urlName]),
                    flagUrl = '?';
                if (url.indexOf('?') != '-1') {
                    flagUrl = '&';
                }
                result = getPath(array[urlName]) + flagUrl + result;
            }
        }
        return result;
    };
    /** 
     * title:参数json合并成一个url  
     * json:普通一级json参数
     * @author kebai
     * @date 2016-6-29
     **/
    that.mergeUrl = function(url, json) {
        if (!url) {
            return '';
        }
        var flagUrl;
        if (url.indexOf('?') != '-1') {
            flagUrl = '&';
        } else {
            flagUrl = '?';
        }
        if (that.isJson(json)) {
            //序列化json
            json = $.param(json);
        }
        if (json) {
            url = url + flagUrl + json;
        }
        return url;
    };

    /** 
     * title:根据key搜索数据
     * data:数据源，一级json数组 
     * name:比配字段名，默认text,(多个时短号隔开，最多三个)
     * key:搜索关键字
     * flag: 是否精确搜索，默认false，模糊搜索
     * @author kebai
     * @date 2016-4-20
     **/

    that.searchKey = function(data, name, key, flag) {
        var arr = [],
            arr1 = [],
            arr2 = [],
            arr3 = [],
            val;
        name = name || 'text';
        flag = flag || false;
        key = (key + '').toLowerCase(); //防止是数字报错 
        if (!data || data.length == 0) {
            return '';
        }
        var nameArr = name.split(','),
            len = nameArr.length;

        function fnValue(value) {
            //单个字段搜索
            arr = fnArr(value, nameArr[0]);
            if (len == 2) { //两个字段搜索
                arr2 = fnArr(value, nameArr[1]);
                //合并数组
                arr = that.merge(arr, arr2);
            }
            if (len == 3) { //三个字段搜索
                arr2 = fnArr(value, nameArr[1]);
                //合并数组
                arr = that.merge(arr, arr2);
                arr3 = fnArr(value, nameArr[2]);
                //合并数组
                arr = that.merge(arr, arr3);
            }
        }

        function fnArr(value, name) {
            val = (value[name] + '').toLowerCase();
            if (!flag && val.indexOf(key) != -1) {
                arr1.push(value);
            } else if (flag && val == key) {
                arr1.push(value);
            }
            return arr1;
        }
        if (that.isArray(data)) {
            $.each(data, function(i, value) {
                fnValue(value);
            });
        } else {
            fnValue(data);
        }
        return arr;
    };



    /** 
     * title:递归遍历多级树形json,返回每条数据链
     * data:数据源,多级json数据 
     * child:子级字段,默认children 
     * callback:回调函数
     * @author kebai
     * @date 2016-5-23
     **/
    that.recursive = function(data, child, callback) {
        child = child || 'children';
        callback = callback || null;
        var arr = [],
            stop = false;

        function getArray(data, callback) {
            if (!data || data.length == 0) {
                return;
            }
            var children = data[child];
            arr.push(data);
            if (stop) {
                return;
            } //停止循环遍历
            if (!children) {
                if (callback) {
                    stop = callback(arr);
                }
                arr.pop(); //删除后一条数据
                return;
            }
            for (var i = 0; i < children.length; i++) {
                arguments.callee(children[i], callback);
            }
            arr.pop(); //删除后一条数据 
        }
        //判断是普通json还是json数组
        if (that.isArray(data)) {
            $.each(data, function(i, value) {
                getArray(value, callback);
            });
        } else {
            getArray(data, callback);
        }
    };



    /** 
     * title:多级树形菜单的过滤搜索
     * data:数据源，多级json数据 
     * child:子级字段，默认children 
     * name:比配字段名，默认name
     * key:搜索关键字
     * flag: 是否精确搜索，默认false，模糊搜索
     * last：是否只匹配最末端子集一层,默认true
     * @author kebai
     * @date 2016-5-23
     **/
    that.searchTree = function(data, name, key, child, flag, last) {
        if (!data || data.length == 0) {
            return;
        }

        var arrObj, text, arr = [],
            array = [];
        child = child || "children";
        name = name || 'text';
        flag = flag || false;
        last = last == false ? false : true;
        key = (key + '').toLowerCase(); //防止是数字报错 
        data = that.toJson(data);

        //递归遍历
        that.recursive(data, child, callback);

        function callback(data) {
            //获取与其关键字匹配
            function fn(val, index) {
                text = (val + '').toLowerCase();
                if (!flag && key && text.indexOf(key) != -1) {
                    array.push(JSON.stringify(data));
                } else if (flag && text == key) {
                    //获取从当前位置往前的所有数据

                    var newData = [];
                    for (var i = 0; i < index; i++) {
                        if (data[i]) {
                            newData.push(data[i]);
                        }
                    }

                    //多级json只有转字符串才能放进数组
                    array.push(JSON.stringify(newData));
                }
            }

            if (last) {
                fn(data[data.length - 1][name], data.length);
            } else {
                $.each(data, function(i, value) {
                    fn(value[name], i);
                });
            }

        }

        return array; //返回类似二维数组

    };


    /** 
     * title:获取多级Tree指定id值
     * data:数据源，多级json数组 
     * idVal:搜索id值 
     * idKey:比配字段名，默认id
     * child:子级字段，默认children  
     * @author kebai
     * @date 2016-9-19
     **/

    that.getTreeVal = function(data, idVal, idKey, child) {
        idKey = idKey || 'id';
        child = child || 'children';
        var deep, newData;
        if (that.isEmpty(data)) {
            return;
        }
        if (!that.isArray(data)) {
            data = [data];
        }
        for (var i = data.length - 1; i >= 0; i--) {
            newData = data[i];
            if (idVal === newData[idKey]) {
                return newData;
            }
            if (newData[child]) {
                deep = arguments.callee(newData[child], idVal, idKey, child);
                if (deep) {
                    return deep;
                }
            }
        }
    };

    /** 
     * title: 粗略的查找页面内容的搜索，只适用一级显示的内容模糊搜索
     * objParent:父级对象(obj,text的父级)   
     * obj:对象列表
     * unit:列表元素 
     * text:搜索框
     * tips:没有内容时提示标签
     * events:默认keyUp事件 
     * callback:回调函数
     * @author kebai
     * @date 2016-5-31
     */
    that.searchCont = function(objParent, obj, unit, text, events, tips, callback) {
        var $objParent = $(objParent);
        if ($objParent.length == '0') {
            return;
        }
        events = events || 'keyup';
        callback = callback || null;
        var $this, key, $filter, $filter2;
        tips = tips || '<p class="as-no-cont as-text-gray" style="display:none">没有相关的搜索内容</p>';
        $objParent.find(text).off(events).on(events, function() {
            $this = $(this);
            $obj = $this.parents(objParent).find(obj);
            $unit = $this.parents(objParent).find(unit);
            $tips = $obj.find('.as-no-cont');
            if ($tips.length == '0') {
                $obj.append(tips);
                $tips = $obj.find('.as-no-cont');
            }
            key = $.trim($this.val());
            if (key) {
                //配备小写
                key = (key + '').toLowerCase();
                $filter = $unit.hide().filter(":contains(" + key + ")");
                //配备大写
                key = (key + '').toUpperCase();
                $filter2 = $unit.hide().filter(":contains(" + key + ")");
                if ($filter.length > 0 || $filter2.length > 0) {
                    $filter.show();
                    $filter2.show();
                    $tips.hide();
                } else {
                    $tips.show();
                }
            } else {
                $tips.hide();
                $unit.show();
            }
            callback && callback();
        });
    };


    /** 
     * title: 判断手机是水平还是垂直状态 
     * @author kebai
     * @date 2016-1-29
     */
    that.landscape = function() {
        var orientation;
        if (window.orientation == 0 || window.orientation == 180) {
            orientation = 'portrait';
            return false;
        } else if (window.orientation == 90 || window.orientation == -90) {
            orientation = 'landscape';
            return true;
        }
    };

    /** 
     * title: 首字母大写 
     * @author kebai
     * @date 2016-1-29
     */

    that.upcaseFirst = function(str) {
        var first, others, ret;
        first = str.substring(0, 1).toUpperCase();
        others = str.substring(1, str.length);
        ret = first + others;
        return ret;
    };

    /** 
     * title: 回车键触发回调
     * obj:父级对象
     * input:搜索输入框
     * callback:回调函数
     * keyCode:键盘值
     * blurGo：是否焦点离开执行
     * @author kebai
     * @date 2016-4-26
     */

    that.enterKeyDown = function(obj, callback, input, keyCode, blurGo) {
        keyCode = keyCode || 13;
        input = input || '.as-keyword';
        var $obj = $(obj),
            $input = $obj.find(input),
            curKey;
        $(document).off('keydown').on('keydown', function(e) {
            curKey = e.which;
            if (curKey == keyCode && $obj.length > 0) {
                if ($input.length > 0) {
                    if ($input.is(":focus")) {
                        callback && callback($obj, $input);
                        e.preventDefault(); //阻止默认提交
                    }
                } else {
                    callback && callback($obj);
                }
            }
        });

        if (blurGo) { //焦点离开执行  
            $input.off('blur').on('blur', function() {
                callback && callback($obj, $input);
            });
        }

    };


    /** 
     * title:滚动到底部触发回调函数加载数据
     * obj:页面对象
     * noStopFlag:停止加载开关,默认false
     * botH:距离底部高度,默认40触发
     * callback:回调函数
     * @author kebai
     * @date 2016-4-26
     */
    that.scrollUpload = function(obj, callback, noStopFlag, botH) {
        var $obj = $(obj);
        if ($obj.length == '0') {
            return;
        }
        noStopFlag = noStopFlag || false;
        botH = botH || 40;
        $(window).off('scroll').on('scroll', function() {
            var winH = $(this).scrollTop() + $(window).height() + botH;
            if (!noStopFlag && winH >= $(document).height() && $(this).scrollTop() > botH) {
                callback($obj);
            }
        });
    };

    /** 
     * title:长按触发事件
     * obj:对象 
     * time:长按时间,默认1秒
     * callback:回调函数,"true"为长按
     * @author kebai
     * @date 2016-4-26
     */
    that.longPress = function(obj, callback, time) {
        var $obj = $(obj);
        if ($obj.length == '0') {
            return;
        }
        time = time || 1000;
        var timeout = null;
        $obj.mousedown(function(events) {
            events.preventDefault();
            clearTimeout(timeout);
            timeout = setTimeout(function() {
                callback("true", $obj);
            }, time);
            events.stopPropagation();
        });
        $obj.mouseup(function() {
            clearTimeout(timeout);
            callback("false", $obj);
        });
        $obj.mouseout(function() {
            clearTimeout(timeout);
            callback("false", $obj);
        });

    };

    /** 
     * title: 本地存储sessionStorage保存获取为json格式, 
     * type:类型，set：存储，get：获取，remove：删除
     * false:true,sessionStorage存储，false：localStorage存储
     * key：键名
     * val: 键值
     * @author kebai
     * @date 2016-5-3
     */
    that.sessionStorage = function(type, key, val, flag) {
        var win = window,
            result;
        flag = flag == false ? false : true;
        if (!key || key.length == 0) {
            return '';
        }
        if (flag) {
            if (type == "set") {
                val = that.toJson(val, false);
                win.sessionStorage.setItem(key, val);
            } else if (type == "get") {
                result = win.sessionStorage.getItem(key) || '';
                result = that.toJson(result);
                return result;
            } else if (type == "remove") {
                win.sessionStorage.removeItem(key);
            }
        } else {
            if (type == "set") {
                val = that.toJson(val, false);
                win.localStorage.setItem(key, val);
            } else if (type == "get") {
                result = win.localStorage.getItem(key) || '';
                result = that.toJson(result);
                return result;
            } else if (type == "remove") {
                win.localStorage.removeItem(key);
            }
        }
    };
    /** 
     * title: 本地存储localStorage保存获取为json格式, 
     * type:类型，set：存储，get：获取，remove：删除 
     * key：键名
     * val: 键值
     * @author kebai
     * @date 2016-6-6
     */
    that.localStorage = function(type, key, val) {
        return that.sessionStorage(type, key, val, false);
    };

    /** 
     * title: cookie存储数据, 
     * type:类型，set：存储，get：获取，remove：删除,times:保存1小时
     * key：键名
     * val: 键值
     * @author kebai
     * @date 2016-6-6
     */

    that.cookie = function(type, key, val, times) {
        //设置
        function setCookie(name, value, expiresHours) {
            var cookieString = name + "=" + encodeURI(value);
            var date = new Date();
            expiresHours = expiresHours || 1;
            date.setTime(date.getTime() + expiresHours * 60 * 60 * 1000);
            cookieString = cookieString + "; expires=" + date.toGMTString();
            document.cookie = cookieString;
        }
        //获取
        var result = "";

        function getCookie(key) {
            var arr, reg = new RegExp("(^| )" + key + "=([^;]*)(;|$)");
            if (arr = document.cookie.match(reg)) {
                result = decodeURI(arr[2]);
            } else {
                result = "";
            }
        }
        //删除
        function removeCookie(key) {
            setCookie(key, "", -1);
        }
        if (type == 'set') {
            setCookie(key, that.toJson(val, false), times);
        } else if (type == 'get') {
            getCookie(key);
            return that.toJson(result) //返回数据
        } else if (type == 'remove') {
            removeCookie(key);
        }
    };

    /** 
     * title: 获取元素本身
     * obj：对象 
     * @author kebai
     * @date 2016-5-20
     */
    that.getObj = function(obj) {
        var $obj = $(obj);
        if ($obj.length == '0') {
            return '';
        }
        return $('<div></div>').append($obj.eq(0).clone()).html();
    };

    /** 
     * title: json与json字符串转换
     * data：数据
     * flag: 类型,默认true,json字符串转json
     * @author kebai
     * @date 2016-5-23
     */
    that.toJson = function(data, flag) {
        flag = flag === false ? false : true;
        try {
            if (flag) {
                data = JSON.parse(data);
            } else {
                data = JSON.stringify(data);
            }
        } catch (e) {}
        return data;
    };

    /** 
     * title: url参数转成json
     * string：url参数 
     * @author kebai
     * @date 2017-6-24
     */
    that.parToJson = function(string, overwrite) {
        if (string.length == 0) {
            return {};
        } else if (string.indexOf('?') != -1) {
            string = string.substring(string.indexOf('?') + 1, string.length);
        }

        var json = {},
            pairs = string.split('&'),
            decode = decodeURIComponent,
            name,
            value;
        $.each(pairs, function(i, pair) {
            pair = pair.split('=');
            name = decode(pair[0]);
            value = decode(pair[1]);
            json[name] = overwrite || !json[name] ? value : [].concat(json[name]).concat(value);
        });
        return json;
    };

    /** 
     * title: 获取对象标签值
     * objParent:父级对象
     * obj:对象
     * dataValue：获取目标标签参数，默认'data-value'(对表单失效,也只能是一级)
     * type: 是否是表单类型，默认false;表单:ture：返回对象标签里的表单数据(name不能有相同的)
     * flag: 数据类型,默认true数组，false：逗号分隔的字符串(或者单个值,对表单失效)
     * text: 是否返回标签文本，默认false;true返回:{"id":,"text":}
     * @author kebai
     * @date 2016-5-26
     */
    that.getValue = function(objParent, obj, dataValue, type, flag, text) {
        var $objParent = $(objParent);
        if ($objParent.length == '0') {
            return '';
        }
        var result, data, name, val, textVal, $this, json = {},
            arr = [],
            arr2 = [];
        dataValue = dataValue || 'data-value';
        type = type || false;
        flag = flag === false ? false : true;
        text = text || false;
        if (!type) {
            $objParent.find(obj).each(function() {
                $this = $(this);
                data = $this.attr(dataValue);
                data && arr.push(that.toJson(data));
                textVal = $this.text();
                textVal && arr2.push(textVal);

            });

            if (!flag) {
                arr = arr.join(',');
                arr2 = arr2.join(',');
            }
            if (!text) {
                result = arr;
            } else {
                result = {
                    "id": arr,
                    "text": arr2
                };
            }
        } else {
            $objParent.find(obj).each(function() {
                $this = $(this);
                name = $this.attr('name');
                val = $.trim($this.val());
                json[name] = val;
                result = $.extend({}, json);
            });
        }

        return result;
    };

    /** 
     * title: 获取多级div树形结构的所有层级焦点值
     * @author kebai
     * @date 2017-10-10
     */
    that.getDataLevel = function(options) {
        var defaults = {
            parentObj: ".as-item-list", //父级对象最好是唯一的
            obj: ".as-dl-list", //对象列表
            item: ".active", //焦点对象
            dataValue: "data-value", //焦点对象标签值
            callback: function(array, $parent, index) {} //回调函数
        };
        options = $.extend(defaults, options);
        var array = [],
            $parent, $this, value, $parentObj = $(options.parentObj);
        if ($parentObj.length == 0) { return; }
        $parentObj.each(function(index) {
            array = [];
            $parent = $(this);
            $parent.find(options.obj).each(function() {
                $this = $(this);
                value = that.getValue($this, options.item, options.dataValue);
                if (value.length > 0) {
                    array.push(value.join(','));
                }
            });
            if (array.length > 0) {
                options.callback(array, $parent, index);
            }
        });
        return array; //父级对象唯一性
    };

    /** 
     * title: 秒、分、时、天、倒计时，秒定时器
     * objParent: 父级对象
     * ms:秒，默认120秒
     * type:类型，默认1为秒，其它则天时分秒倒计时
     * callback:倒计时完成后回调函数
     * @author kebai
     * @date 2016-6-7
     */

    that.timer = function(objParent, callback, ms, type, objs, objm, objh, objd) {
        var win = window,
            setT = null;
        // if ($(objParent).length == '0') {
        //     return;
        // }
        objs = objs || '#timer-second'; //秒
        objm = objm || '#timer-minute'; //分
        objh = objh || '#timer-hour'; //时
        objd = objd || '#timer-day'; //天
        var $objs = $(objs),
            $objm = $(objm),
            $objh = $(objh),
            $objd = $(objd);
        var day, hour, minute, second;
        ms = Number(ms) || 120;
        type = type || 1;
        if (type == 1) {
            clearInterval(setT);
            setT = win.setInterval(function() {
                second = 0;
                if (ms > 0) {
                    second = ms;
                } else {
                    clearInterval(setT);
                    setT = null;
                    second = 0;
                    callback();
                    $(objs).html(0);
                }
                second = second < 10 ? '0' + second : second;
                $objs.html(second);
                ms--;
            }, 1000);

        } else {
            clearInterval(setT);
            setT = win.setInterval(function() {
                day = 0, hour = 0, minute = 0, second = 0;
                if (ms > 0) {

                    day = Math.floor(ms / (60 * 60 * 24));
                    hour = Math.floor(ms / (60 * 60)) - (day * 24);
                    minute = Math.floor(ms / 60) - (day * 24 * 60) - (hour * 60);
                    second = Math.floor(ms) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60);

                } else {
                    clearInterval(setT);
                    setT = null;
                    day = 0, hour = 0, minute = 0, second = 0;
                    callback();
                    $(objd, objh, objm, objs).html(0);
                }
                day = day < 10 ? '0' + day : day;
                hour = hour < 10 ? '0' + hour : hour;
                minute = minute < 10 ? '0' + minute : minute;
                second = second < 10 ? '0' + second : second;
                $objd.html(day);
                $objh.html(hour);
                $objm.html(minute);
                $objs.html(second);
                ms--;
            }, 1000);
        }
        return setT;
    };



    /** 
     * title: 循环定时器（对setInterval的替代优化）
     * callback:完成后回调函数 
     * time:间隔时间，默认1000毫秒
     * @author kebai
     * @date 2016-11-3
     */
    that.cycleTimer = function(callback, time) {
        time = time || 1000;
        var setT = null,
            stop = false,
            fnInter;

        //对setInterval的替代优化 
        function interval(func, time) {
            fnInter = function() {
                stop = func();
                clearTimeout(setT);
                if (stop) { return; }
                setT = setTimeout(fnInter, time);
            };
            clearTimeout(setT);
            setT = setTimeout(fnInter, time);
        }

        interval(function() {
            return callback(); //回调函数返回true则停止定时器循环
        }, time);
    };

    /** 
     * title: 监听storage改变值事情获取子页面保存成功后刷新其父级grid列表 
     * grid: grid 对象,若不是列表传null。
     * reloadFlag,子页面时保存成功时设置其本地存储值为true(that.localStorage('set', 'reloadFlag',true)。
     * callback:完成后回调函数
     * reloadFlag:保存的标识字段,默认reloadFlag 
     * @author kebai
     * @date 2017-5-22
     */
    that.parentReload = function(grid, callback, reloadFlag) {
        reloadFlag = reloadFlag || 'reloadFlag';
        if (that.localStorage('get', reloadFlag) + '' == 'true') { //初始化刷新的默认标识
            that.localStorage('remove', reloadFlag);
        }
        $(window).on('storage', function(e) {
            onStorageChange(e.originalEvent)
        });

        function onStorageChange(e) {
            if (e.key == reloadFlag) {
                //grid列表刷新
                if (grid) { grid.reload(); }
                callback && callback(that.toJson(e.newValue), e);
                //清除数据以便实时更新
                that.localStorage('remove', reloadFlag);
            }
        }
    };


    /** 
     * title: 对话框的表单提交
     * obj:表单对象; 
     * url:接口路径
     * msg:提交时提示信息
     * status:状态码，默认1成功
     * callback:回调函数 
     * @author kebai
     * @date 2016-6-8
     **/
    that.dialogForm = function(obj, url, dialogName, callback, msg, status) {
        var $obj = $(obj);
        if ($obj.length == '0') {
            return;
        }
        var data = that.formJson($obj);
        callback = callback || null;
        status = status || 1;
        msg = msg || '正在提交...';
        that.dialogTipsMsg(msg, false, true);
        that.ajaxData($obj, url, function($obj, result) {
            if (result.status == status || result.status == 'success') {
                var msg = result.message || '提交成功';
                that.dialogTipsMsg(msg); //成功提示
                dialogName && dialogName.close(); //关闭对话框  
                callback && callback(result);
            } else {
                that.dialogTipsMsg(result.message); //失败提示   
            }
        }, data, "post");
    };


    /** 
     * title: 复选框，全选(1)、全不选(2)、反选(3)
     * checkbox:复选框; 
     * checklist: 复选框列表对象,默认'.check-list :checkbox'; 
     * type: 类型，默认1全选
     * @author kebai
     * @date 2016-6-14
     **/
    that.checkAll = function(checkbox, type, checklist, events) {
        var $checkbox = $(checkbox);
        type = type || 1;
        checklist = checklist || '.check-list :checkbox';
        events = events || 'click';
        if (type == 1) {
            $checkbox.off(events).on(events, function() { //全选 
                if ($(this).is(":checked")) {
                    $(checklist).prop("checked", true);
                } else {
                    $(checklist).prop("checked", false);
                }
            });
        } else if (type == 2) {
            $checkbox.off(events).on(events, function() { //全不选
                $(checklist).prop("checked", false);
            });
        } else if (type == 3) {
            $checkbox.off(events).on(events, function() { //反选
                $(checklist).each(function() {
                    $(this).prop("checked", !$(this).prop("checked"));
                });
            });
        }

    };


    /** 
     * title: 输入框焦点文字 placeholder兼容处理 （提交用formJson）
     * @author kebai
     * @date 2016-6-24
     **/
    that.placeholder = function() {
        //判断浏览器是否支持placeholder属性
        var supportPlaceholder = 'placeholder' in document.createElement('input');
        var placeholder = function(input) {
            var text = input.attr('placeholder'),
                defaultValue = input.defaultValue;
            if (!defaultValue) {
                input.val(text).addClass("as-phcolor");
            }
            input.focus(function() {
                if (input.val() == text) {
                    $(this).val("");
                }
            });
            input.blur(function() {
                if (input.val() == "") {
                    $(this).val(text).addClass("as-phcolor");
                }
            });
            //输入的字符不为灰色
            input.keydown(function() {
                $(this).removeClass("as-phcolor");
            });
        };
        //当浏览器不支持placeholder属性时，调用placeholder函数
        if (!supportPlaceholder) { //ie9存在bug
            $('input').each(function() {
                if ($(this).attr("type") == "text" && $(this).attr("placeholder")) {
                    placeholder($(this));
                }
            });
        }
    };

    /** 
     * title: json拆分为json数组(应用于grid表头)
     * length:数组最大长度，默认取field长度
     * {"id":["id1","id2"],"name":["a1","a2"]}==>[{"id":"id1","name":"a1"},{"id":"id2","name":"a2"}] 
     * @author kebai
     * @date 2016-7-7
     **/
    that.jsonArray = function(json, length) {
        length = length || json.field.length;
        var nameArr = [],
            data = {},
            jsonArr = [],
            flag = 0;
        for (var i in json) {
            nameArr.push(i);
            if (i == 'name') {
                flag = 1;
            }
        }
        for (var i = 0; i < length; i++) {
            data = {};
            for (var j = 0; j < nameArr.length; j++) {
                if (json[nameArr[j]] && json[nameArr[j]][i] || json[nameArr[j]] && json[nameArr[j]][i] == false) {
                    if (!flag) {
                        data.name = 'id' + i;
                    } //显示隐藏列需要name字段
                    data[nameArr[j]] = json[nameArr[j]][i];
                }
            }
            if (!that.isEmpty(data)) {
                jsonArr.push(data);
            }

        }
        return jsonArr;
    };


    /** 
     * title: 通过url下载文件
     * @author kebai
     * @date 2016-9-10
     **/
    that.download = function(url) {
        if (url.length == '0') {
            return;
        }
        var str = '<iframe src="" style="display:none" id="iframe-download"></iframe>';
        if ($('#iframe-download').length == '0') {
            $('body').append(str);
        }
        $('#iframe-download').attr('src', url);
    };

    /** 
     * title: 通过iframe打开url文件,全部展开iframe内容高度
     * obj:iframe 父级容器 
     * url:文件路径
     * height:iframe 默认高度 450
     * callback:回调函数
     * @author kebai
     * @date 2016-12-30
     **/
    that.iframeOpen = function(obj, url, height, callback) {
        if ($(obj).length == '0') {
            return;
        }
        height = height || 450;
        var str = '<iframe src="' + url + '" id="iframe-file" name="iframeFile" width="100%" style="z-index:0" height="' + height + '" frameborder="no" border="0" marginwidth="0" marginheight="0" scrolling="yes" ></iframe>';
        var $iframe = $('#iframe-file');
        if ($iframe.length == '0') {
            $(obj).append(str);
            $iframe = $('#iframe-file');
        } else {
            $iframe.attr('src', url);
        }
        $iframe.on('load', function() {
            var $doc = $(window.frames["iframeFile"].document);
            var docH = $doc.height();
            $iframe.css("height", docH);
            callback && callback($iframe, docH, $doc);
        });
    };


    /** 
     * title: 瀑布流布局,返回最小高度的列对象
     * @author kebai
     * @date 2016-11-8
     **/
    that.waterfall = function(options) {
        // 默认值
        var defaults = {
            waterParent: "#water-wrap", //父级标签
            waterItem: ".water-item", //列元素标签
            waterDiv: ".water-div", //内容选项标签
            waterNumber: 4, //默认4列
            callback: function() {} //回调函数
        };
        options = $.extend(defaults, options);
        var $obj, $waterParent = $(options.waterParent),
            callback = options.callback;

        if ($waterParent.length == '0') {
            return;
        }

        //是否已经存在列元素，没有则创建
        if ($waterParent.find(options.waterItem).length == 0) {
            var str = '<div class="' + options.waterItem.replace(/\./g, "") + '"></div>';
            for (var i = options.waterNumber; i > 0; i--) {
                $waterParent.append(str);
            }
            $waterParent.find(options.waterItem).css({
                "width": (100 / options.waterNumber) + '%',
                "float": "left"
            });
        }
        $obj = $waterParent.find(options.waterItem);
        //获取最低高度的列元素 
        var arr = [],
            tempH = 1000000,
            $this, itemH, $minObj;
        $obj.each(function() {
            $this = $(this);
            itemH = $(this).height();
            if (tempH > itemH) {
                tempH = itemH;
                $minObj = $this;
            }
        });
        callback($minObj, $waterParent);
    };


    /** 
     * title: 打印指定内容
     * obj:id对象，默认print-doc
     * @author kebai
     * @date 2016-11-17
     **/
    that.print = function(obj, time) {
        obj = obj || 'print-doc';
        time = time || 200;
        obj = obj.replace(/\.|\#/g, '');
        var newstr = document.all.item(obj).innerHTML;
        if (!newstr) {
            return;
        }
        var newWin = window.open();
        newWin.document.write(newstr);
        newWin.document.close(); //解决ie下问题
        setTimeout(function() { //延时等待渲染
            newWin.focus();
            newWin.print();
            newWin.close();
        }, time);
    };



    /** 
     * title: 通过url播放MP3(只支持MP3)
     * @author kebai
     * @date 2016-9-29
     **/

    that.playSound = function(url) {
        if ($('#bell-sound').length == '0') {
            $('body').append('<div id="bell-sound"></div>');
        }
        var $obj = $('#bell-sound');
        $obj.hide();
        var ua = navigator.userAgent.toLowerCase();
        if (ua.match(/msie ([\d.]+)/)) {
            $obj.html('<object classid="clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95"><param name="AutoStart" value="1" /><param name="Src" value="' + url + '" /></object>');
        } else if (ua.match(/firefox\/([\d.]+)/)) {
            $obj.html('<embed src="' + url + '" type="audio/mp3" hidden="true" loop="false" mastersound></embed>');
        } else if (ua.match(/chrome\/([\d.]+)/)) {
            $obj.html('<audio src="' + url + '" type="audio/mp3" autoplay="autoplay" hidden="true"></audio>');
        } else if (ua.match(/opera.([\d.]+)/)) {
            $obj.html('<embed src="' + url + '" hidden="true" loop="false"><noembed><bgsounds src="' + url + '"></noembed>');
        } else if (ua.match(/version\/([\d.]+).*safari/)) {
            $obj.html('<audio src="' + url + '" type="audio/mp3" autoplay="autoplay" hidden="true"></audio>');
        } else {
            $obj.html('<embed src="' + url + '" type="audio/mp3" hidden="true" loop="false" mastersound></embed>');
        }
    };

    /**
     * title: 设置左右两边高度,设置内容滚动条(可以只设置一边)
     * @author kebai
     * @date 2017-03-06
     **/
    that.setHeight = function(options) {
        options = options || {};

        function fnH(options) {
            var defaults = {
                leftBox: ".as-sub-menu .as-cont", //左边菜单
                leftCont: ".mini-grid-rows-view", //左边tree内容
                rightBox: ".as-grid", //右边模块
                rightCont: ".mini-grid-rows-view", //右边grid列表
                ltTop: 20, //误差值
                rtTop: 50, //误差值
                callback: function() {} //回调函数
            };
            var opts = $.extend(defaults, options);
            var ltTop = opts.ltTop,
                rtTop = opts.rtTop,
                callback = opts.callback;
            var winH, leftH, rightH, $leftBox, $rightBox, $leftCont, $rightCont, leftTop, rightTop;
            winH = $(window).height();
            $leftBox = $(opts.leftBox);
            $rightBox = $(opts.rightBox);

            function fnSet() {
                $leftCont = $leftBox.find(opts.leftCont);
                $rightCont = $rightBox.find(opts.rightCont);
                leftTop = $leftCont.length > 0 ? $leftCont.offset().top : $('.as-head').height();
                rightTop = $rightCont.length > 0 ? $rightCont.offset().top : $('.as-head').height();
                leftH = winH - leftTop - ltTop;
                leftH = leftH > 50 ? leftH : 50;
                rightH = winH - rightTop - rtTop;
                rightH = rightH > 50 ? rightH : 50;

                $leftCont.css({ "height": leftH, "overflowY": "auto" });
                $rightCont.css({ "height": rightH, "overflowY": "auto" });
                $('.as-sub-menu').css({ "minHeight": "auto" });
                callback(leftH, rightH, $leftCont, $rightCont);
            }
            clearTimeout(setT);
            setT = setTimeout(function() {
                fnSet();
            }, 60);
        }
        var setT = null;
        fnH(options);
        $(window).on('resize', function() {
            fnH(options);
        });
        $('#as-unfold-btn').on('click', function() {
            fnH(options);
        })
    };

    /**
     * title: 滚动到指定位置后固定导航（顶部）
     * @author kebai
     * @date 2017-03-23
     **/
    that.posfixed = function(options) {
        options = options || {};
        var defaults = {
            obj: "#posfixed", //固定对象
            distance: 0, //距顶部距离
            zIndex: 1000 //层次
        };
        options = $.extend(defaults, options);
        var $obj = $(options.obj);
        if ($obj.length == '0') {
            return;
        }
        var initPos = $obj.offset().top,
            initH = $obj.height(),
            initW = $obj.width();
        var $win = $(window),
            flag = 0,
            str, objTop, $warp;
        str = '<div class="pos-fixed-wrap" style="position:fixed;z-index:' + options.zIndex + '"></div>';
        $win.scroll(function(event) {
            fnfixed(); //滚动
        });
        $win.resize(function(event) {
            fnfixed(); //缩放
        });

        function fnfixed() {
            objTop = $obj.offset().top - $win.scrollTop();
            if (objTop <= options.distance) {
                if (!flag) { //只追加一次
                    $obj.wrap(str);
                    //空标签填充
                    $obj.parents('.pos-fixed-wrap').after('<div class="pos-fixed-space" style="height:' + initH + 'px;width:' + initW + 'px"></div>');
                    $warp = $obj.parents('.pos-fixed-wrap');
                    $obj.css('width', initW);
                    flag = 1;
                }
                $warp.css("top", options.distance);
            }
            if ($obj.offset().top <= initPos) {
                $('.pos-fixed-wrap').after($obj);
                $obj.siblings('.pos-fixed-wrap,.pos-fixed-space').remove();
                flag = 0;
            }
        }

    };



    /**
     * title: select多级下拉联动事件(二级、三级、四级)
     * @author kebai
     * @date 2017-08-17
     **/
    that.selectLinkage = function(options) {
        var defaults = {
            parentObj: '.select-box', //它们的父级对象
            sel1: '.select-1',
            sel2: '.select-2',
            sel3: '.select-3',
            sel4: '.select-4',
            keyName: 'id', //url参数字段
            data: 'data', //接口数据字段
            dataValue: 'data-parentid', //传的标签值,若为空则为选中的select值
            dataUrl: 'data-url', //接口路径
            selTmpl: '#select-tmpl', //模板对象
            flag: true, //是否初始化下拉,默认初始化
            value: 'value', //初始化标签显示值
            width: '100%', //宽度
            callback: function($sel, num, result) {} //回调函数
        }
        options = $.extend(defaults, options);
        var $sel1 = $(options.sel1),
            $sel2 = $(options.sel2),
            $sel3 = $(options.sel3),
            $sel4 = $(options.sel4),
            callback = options.callback;
        var url, $this, $parent, id, setT = null,
            arr = [1, 2, 3, 4];
        //初始化下拉
        if (options.flag) {
            $sel1.each(function() {
                fnSet($(this), 1, 1);
            });
        }
        //联动下拉
        $sel1.on("change", function() {
            fnSet($(this), 2);
        });
        $sel2.on("change", function() {
            fnSet($(this), 3);
        });
        $sel3.on("change", function() {
            fnSet($(this), 4);
        });

        //联动
        function fnSet($this, num, flag) {
            $parent = $this.parents(options.parentObj);
            $sel = $parent.find(options["sel" + num]);
            if ($sel.length == 0) {
                return;
            }
            value = $sel.attr(options.value);
            if (flag == 1) { //只初始化
                url = $sel.attr(options.dataUrl);
                fnNextSel($sel, url, value, num);
                if (!value) {
                    fnArr(num);
                }
                return;
            }
            id = $this.find('option:selected').attr(options.dataValue) || $this.find('option:selected').val() || "";
            if (id != "") {
                url = $sel.attr(options.dataUrl);
                id = encodeURI(id);
                url = that.changeURL(options.keyName, id, url);
                if (flag == 2) { //初始化联动
                    fnNextSel($sel, url, value, num);
                    if (!value) {
                        fnArr(num);
                    }
                } else { //选择联动
                    fnNextSel($sel, url, '', num);
                    fnArr(num);
                }

            } else {
                fnArr(num - 1); //选择为空时
                callback($this, num);
            }
        }

        function fnArr(num) {
            var arr2 = arr.slice(num);
            $.each(arr2, function(i, val) {
                $sel = $parent.find(options["sel" + val]);
                $sel.find('option').eq(0).prop("selected", true);
                $sel.attr("disabled", true);
                fnTry($sel);
            });
        }

        function fnTry($obj) { //下拉插件
            try {
                $obj.chosen({ "width": options.width });
                $obj.trigger("chosen:updated");
            } catch (e) {}
        }
        //请求数据
        function fnNextSel($obj, url, value, num) {
            $obj.prop("disabled", false);
            that.ajaxData($obj, url, fnCallback);

            function fnCallback($obj, result) {
                if (!that.isEmpty(result[options.data])) {
                    result.value = value; //初始化值
                    that.tmpl($obj, result, options.selTmpl, function() {
                        if (value) {
                            fnSet($obj, num + 1, 2); //初始化设置下一个下拉联动
                        } else {
                            $obj.find('option').eq(0).prop("selected", true);
                        }
                        fnTry($obj);
                    });
                } else {
                    $obj.html('');
                    fnTry($obj);
                }
                callback($obj, num, result);
            }
        }
    };

    /**
     * title: 重新初始化标签(规避jq事件里的事件重复执行问题、只支持原始的div标签,但miniui等插件不支持)
     * obj: 标签对象(确认对象的唯一性、否则影响其他正常事件)
     * @author kebai
     * @date 2017-09-26
     **/
    that.initHtml = function(obj) {
        var $obj = $(obj),
            $this;
        $obj.each(function() {
            $this = $(this);
            $this.wrap('<div class="init-temp-wrap"></div>');
            $this.parents('.init-temp-wrap').html(that.getObj($this));
            $(this).unwrap();
        });
    };

    /**
     * title: 双击文本标签编辑文本
     * parentObj:父级对象
     * obj:文本标签对象
     * callback:回调函数
     * event:默认双击事件'dblclick'
     * @author kebai
     * @date 2017-10-10
     **/
    that.dblclickEdit = function(parentObj, obj, callback, event) {
        event = event || 'dblclick';
        $(parentObj).off(event).on(event, obj, function() {
            var $this = $(this),
                text = $.trim($this.text()),
                newText, $input;
            $input = $("<input type='text' class='as-input db-edit-text'/>").val(text);
            if ($('.db-edit-text').length > 0) { return; } //已经存在不重复添加
            $this.text('');
            $this.append($input);
            //选中文本
            $input.get(0).setSelectionRange && $input.get(0).setSelectionRange(0, text.length);
            $input.focus();

            $input.on('blur', function() {
                newText = $.trim($(this).val());
                newText = newText ? newText : text;
                //移除文本框,显示新值
                $(this).remove();
                $this.text(newText);
                if (newText != text) { //值改变才回调
                    callback && callback($this, newText, text);
                }
            });
        });
    };



    /*===============插件的二次封装===============*/

    /** 
     * title: 对artDialog对话框的封装,提示信息 
     * msg:提示信息;  
     * noShowBg:是否显示背景遮罩,默认false显示遮罩
     * noClose:不关闭，默认false自动关闭
     * time:自动隐藏时间，默认1.5秒 
     * tipsId:对话框id
     * @author kebai
     * @date 2016-3-29
     **/

    that.dialogTipsMsg = function(msg, noShowBg, noClose, time, tipsId) {
        if (that.isEmpty(msg)) { //接口成功为空
            msg = '请求成功';
        }
        time = time || 1500;
        noShowBg = noShowBg || false;
        noClose = noClose || false;
        tipsId = tipsId || 'tipsId';
        var setT = null,
            dialogName, content;
        dialogName = dialog({
            id: tipsId
        });

        dialogName.addEventListener('close', function() {
            dialogName.close().remove(); //销毁对话框
        });

        var icon = 'icon-warning';
        if (noClose) { //正在提交...
            icon = 'as-loading';
        }
        if (msg.indexOf('成功') != -1) {
            icon = 'icon-normal';
        }
        if (msg.indexOf('失败') != -1) {
            icon = 'icon-delete2';
        }
        content = '<span class="as-tips-msg"><i class="as-icon ' + icon + '"></i>' + msg + '</span>';
        dialogName.content(content);

        if (!noShowBg) {
            dialogName.showModal();
        } else {
            dialogName.show();
        }
        if (!noClose) {
            clearTimeout(setT);
            setT = setTimeout(function() {
                dialogName.close().remove();
            }, time);
        }
        return dialogName;
    };

    /** 
     * title: 对artDialog对话框的封装,点击关闭对话框
     * obj:关闭按钮obj;  
     * dialogName:对话框名称
     * flag:隐藏还是彻底删除，默认true销毁 
     * events:事件关闭，默认点击事件
     * @author kebai
     * @date 2016-3-29
     **/
    that.dialogColse = function(obj, dialogName, flag, events) {
        var $obj = $(obj);
        if ($obj.length == '0' || !dialogName) {
            return;
        }
        flag = flag || true;
        events = events || "click";
        $obj.off(events).on(events, function(event) {
            if (flag) {
                dialogName.close().remove();
            } else {
                dialogName.close();
            }
            event.stopPropagation(); //阻止冒泡事件
        });
        if (flag) {
            $('.ui-dialog-close').off('click').on('click', function() {
                dialogName.close().remove();
            });
        }

    };

    /** 
     * title: 对artDialog对话框显示确定与取消按钮的封装 
     * content:内容，title：标题，width：宽度
     * callback:确定的回调函数
     * noShowBg:是否显示背景遮罩,默认false显示遮罩 
     * dialogId:对话框唯一id
     * zIndex:层次
     * @author kebai
     * @date 2016-6-6
     **/
    that.dialogShow = function(title, content, callback, width, noShowBg, dialogId, zIndex) {
        if ($(content).length == '0') {
            content = '<p class="as-d-tips"><i class="as-icon"></i>' + content + '</p>';
        }
        var $obj = $(content);
        title = title || '';
        width = width || 300;
        noShowBg = noShowBg || false;
        var dialogName;
        var defaults = {
            title: title,
            content: $obj,
            width: width,
            okValue: '确定',
            ok: function() {
                var $dObj = $obj.parents('.ui-dialog');
                callback && callback(dialogName, $dObj);
                return false;
            },
            cancelValue: '取消',
            cancel: function() {}
        };
        if (dialogId) {
            defaults.id = dialogId;
        }
        if (zIndex) {
            defaults.zIndex = zIndex;
        }

        dialogName = dialog(defaults);

        var dH = $(window).height() - 80; //设置最大高度
        dH = dH > 420 ? dH : 420;
        $('.ui-dialog-content').css({ 'maxHeight': dH, 'overflow-y': 'auto' });

        dialogName.addEventListener('close', function() {
            dialogName.close().remove(); //销毁对话框 
        });

        if (!noShowBg) {
            dialogName.showModal();
        } else {
            dialogName.show();
        }
        return dialogName;
    };

    that.dialogShowNext = function(title, content, callback, width, noShowBg, dialogId, zIndex) {
        if ($(content).length == '0') {
            content = '<p class="as-d-tips"><i class="as-icon"></i>' + content + '</p>';
        }
        var $obj = $(content);
        title = title || '';
        width = width || 300;
        noShowBg = noShowBg || false;
        var dialogName;
        var defaults = {
            title: title,
            content: $obj,
            width: width,
            okValue: '下一步',
            ok: function() {
                var $dObj = $obj.parents('.ui-dialog');
                callback && callback(dialogName, $dObj);
                return false;
            },
            cancelValue: '取消',
            cancel: function() {}
        };
        if (dialogId) {
            defaults.id = dialogId;
        }
        if (zIndex) {
            defaults.zIndex = zIndex;
        }

        dialogName = dialog(defaults);

        var dH = $(window).height() - 80; //设置最大高度
        dH = dH > 420 ? dH : 420;
        $('.ui-dialog-content').css({ 'maxHeight': dH, 'overflow-y': 'auto' });

        dialogName.addEventListener('close', function() {
            dialogName.close().remove(); //销毁对话框
        });

        if (!noShowBg) {
            dialogName.showModal();
        } else {
            dialogName.show();
        }
        return dialogName;




    };

    /** 
     * title: 对artDialog对话框引入div对象的封装
     * obj:对象; 
     * closebtn：关闭按钮,默认'.as-cancel-btn'
     * title：标题，width：宽度,默认520
     * callback:回调函数
     * noShowBg:是否显示背景遮罩,默认false显示遮罩  
     * zIndex:层次
     * @author kebai
     * @date 2016-6-8
     **/
    that.dialog = function(obj, title, closebtn, width, callback, noShowBg, zIndex) {
        var $obj = $(obj);
        if ($obj.length == '0') {
            return;
        }
        title = title || '';
        width = width || 520;
        closebtn = closebtn || '.as-cancel-btn';
        noShowBg = noShowBg || false;
        callback = callback || null;
        var _id = $obj.selector || obj;
        var defaults = {
            id: _id,
            content: $obj,
            width: width
        };
        if (zIndex) {
            defaults.zIndex = zIndex;
        }
        var dialogName = dialog(defaults);
        dialogName.title(title);

        var dH = $(window).height() - 50; //设置最大高度
        dH = dH > 420 ? dH : 420;
        $('.ui-dialog-content').css({ 'maxHeight': dH, 'overflow-y': 'auto' });

        dialogName.addEventListener('close', function() {
            dialogName.close().remove(); //销毁对话框 
        });

        if (!noShowBg) {
            dialogName.showModal();
        } else {
            dialogName.show();
        }
        that.dialogColse($obj.find(closebtn), dialogName);
        callback && callback();
        return dialogName;
    };


    /** 
     * title: 新的对话框弹窗(url弹窗、内容标签弹窗，二选一) 
     * @author kebai
     * @date 2017-09-28
     **/
    that.dialogBox = function(options) {

        var defaults = {
            id: 'd-iframe',
            title: '标题', //标题
            url: null, //url弹窗
            content: null, //内容标签弹窗
            closebtn: '.as-cancel-btn', //关闭按钮 
            width: 850, //宽度
            height: 420, //高度
            noShowBg: false, //是否显示背景遮罩
            fixed: true, //是否固定
            flag: true, //是否显示确定取消按钮
            topFlag: true, //是否顶级窗口弹窗
            callback: function(dialogName) {} //回调函数
        };

        var opts = $.extend(true, defaults, options);
        var callback = opts.callback,
            setT = null,
            $obj = $(opts.content),
            top$ = $;

        if (window.self != window.top && window.parent && opts.topFlag && window.top.dialog) { //在iframe页面顶级弹窗(并防止内嵌别的系统内)
            dialog = window.top.dialog;
            $obj = $obj.clone(); //克隆复制对象 
            top$ = window.top.$; //设置顶层窗口top$对象
        }

        if (opts.flag) {
            var defBtn = {
                okValue: '确定',
                ok: function() {
                    var $dObj = $obj.length > 0 ? $obj.parents('.ui-dialog') : top$('.ui-dialog'); //若使用top$验证会报错
                    callback && callback(dialogName, $dObj);
                    return false;
                },
                cancelValue: '取消',
                cancel: function() {}
            }
            opts = $.extend(true, defBtn, opts);
        }

        var dialogName = dialog(opts);
        var $iframe = top$('.ui-dialog-content').find('iframe');
        $iframe.attr('scrolling', 'auto').hide();
        var dH = top$(window).height() - 50; //设置最大高度
        dH = dH > 420 ? dH : 420;
        dH = dH > opts.height ? opts.height : dH;

        dialogName.addEventListener('close', function() {
            dialogName.close().remove(); //销毁对话框 
            top$("iframe[src='about:blank']").empty().remove();
            if (window.self != window.top && window.parent && window.top.dialog) {
                top$($obj).remove(); //移除克隆对象
            }
        });

        if (!opts.noShowBg) {
            dialogName.showModal();
        } else {
            dialogName.show();
        }

        that.dialogColse(top$(opts.closebtn), dialogName);

        if (opts.url) {
            top$('.ui-dialog-content').css({ 'height': dH, 'maxHeight': '100%', 'padding': '0px' });
            $iframe.attr('height', dH);
            $iframe.on('load', function() {
                $iframe.contents().find('html,body').css({ 'height': dH, 'overflow': 'auto' });
                clearTimeout(setT);
                setT = setTimeout(function() { //解决闪烁问题
                    $iframe.show();
                }, 10);
                that.dialogColse($iframe.contents().find(opts.closebtn), dialogName);
                if (!opts.flag) { callback && callback(dialogName); }
            })
        } else {
            top$('.ui-dialog-content').css({ 'maxHeight': dH, 'overflow': 'auto' });
        }
        return dialogName;
    };

    /** 
     * title: 对pagination分页插件的封装,动态ajax请求分页、静态数据分页(返回当前页的数据)
     * @author kebai
     * @date 2016-6-28
     **/
    that.pagination = function(options) {
        var defaults = {
            pageObj: ".page-box", //分页页码对象
            url: null, //接口路径
            data: null, //静态数据
            pageType: false, //是否动态分页，默认false为静态数据分页,true为动态分页
            param: {}, //请求参数
            total: "total", //总数字段 
            dataList: "data", //数据列表字段
            pageIndex: "pageIndex", //页码字段
            pageSize: "pageSize", //每页显示几条字段
            type: "get", //请求方式,默认get
            showPage: false, //是否显示输入框和页码数,默认false不显示(对象pageObj必须是唯一的否则有问题)
            /*分页插件参数*/
            items_per_page: 15, //每页显示多少条 
            num_edge_entries: 0,
            num_display_entries: 0,
            current_page: 0, //当前第几页
            callback: function() {}, //回调函数  
            link_to: "javascript:;",
            prev_text: " ",
            next_text: " ",
            ellipse_text: "..."
        };
        options = $.extend(defaults, options);
        var $pageObj = $(options.pageObj),
            callback = options.callback;
        var param = options.param,
            perPage = options.items_per_page;
        if ($pageObj.length == '0') {
            return;
        }

        if (options.pageType) { //动态分页，返回每次请求的结果

            param[options.pageSize] = perPage;
            var total = 0,
                flag = 0,
                setT = null,
                newCallback; //控制开关 

            function fnAjax(index) {
                param[options.pageIndex] = index;
                that.ajaxData($pageObj, options.url, function($obj, result) {
                    total = result[options.total] || 0;
                    if (flag == 0) { //只初始化一次
                        var newCallback = function(index) {
                            if (flag == 0) { //初始化执行

                                fnShowPage(total, perPage);
                                callback(result, index, total, perPage);

                                flag = 1;
                            } else { //单击后执行请求                  
                                fnAjax(index);
                            }
                        };
                        options.callback = newCallback;
                        clearTimeout(setT);
                        setT = setTimeout(function() { // 创建分页 
                            $pageObj.pagination(total, options);
                        }, 50);
                    } else {
                        fnShowPage(total, perPage);
                        callback(result, index, total, perPage);
                    }

                    $pageObj.show();

                }, param, options.type);
            }
            fnAjax(options.current_page);

        } else {
            if (options.url) {
                //传url静态分页
                that.ajaxData($pageObj, options.url, function($obj, result) {
                    fnData(result[options.dataList]);
                }, param, options.type);
            } else if (options.data) {
                //传data静态数据分页  
                fnData(options.data);
            }
        }
        //数据处理  
        function fnData(data) {
            data = data || [];
            var maxNum,
                arr = [],
                result = {},
                setT2 = null;

            function newCallback(index) {
                arr = [];
                //获取当前分页数据
                maxNum = Math.min((index + 1) * perPage, data.length);
                for (var i = index * perPage; i < maxNum; i++) {
                    arr.push(data[i]);
                }
                result[options.dataList] = arr;
                total = data.length;

                fnShowPage(total, perPage);
                //返回当前分页的数据
                callback(result, index, total, perPage);

            }

            options.callback = newCallback;
            clearTimeout(setT2);
            setT2 = setTimeout(function() { // 创建分页
                $pageObj.pagination(data.length, options);
            }, 50);
            $pageObj.show();

        }
        //分页码
        var str, $page, num;

        function fnShowPage(total, perPage) {
            if (!options.showPage) { return; }
            str = '&nbsp;<input type="text" name="key" value="" placeholder="" class="as-inupt as-w30 as-text-ct as-page-index">&nbsp;<span class="as-page-number">每页' + perPage + '条,共' + total + '条</span>';

            $page = $pageObj.next('.as-page-show');
            if ($page.length == 0) {
                $pageObj.after('<span class="as-page-show" style="display:inline-block;font-size:12px;vertical-align:middle;margin-top:10px"></span>');
                $page = $pageObj.next('.as-page-show');
            }
            $page.html(str);

            //输入焦点移开跳转
            that.enterKeyDown($page, function($obj, $input) {
                num = $.trim($input.val());
                if (!that.isNumber(num) || num <= 0 || num > total / perPage) {
                    $input.val('');
                    return;
                }
                options.current_page = num - 1;
                $pageObj.pagination(total, options);
            }, '.as-page-index', null, true);
        }

    };

    /** 
     * title: 对store.min本地存储localStorage设置过期日期
     * type:类型，set：存储，get：获取，remove：删除
     * key：键名
     * val: 键值
     * exp: 过期日期，秒单位，默认为一天
     * @author kebai
     * @date 2016-1-29
     */

    that.storeDate = function(type, key, val, exp) {
        if (type == "set") {
            exp = exp || 24 * 60 * 60 * 1000;
            var json = {
                "value": val,
                "exp": exp,
                "time": new Date().getTime()
            };
            store.set(key, json);
        } else if (type == "get") {
            var data = store.get(key);
            if (!data || new Date().getTime() - data.time > data.exp) {
                store.remove(key);
                return '';
            }
            return data.value;
        } else if (type == "remove") {
            store.remove(key);
        }

    };

    /** 
     * title: grid列表dialog 12气泡提示文字
     * objParent:父级对象
     * obj：对象
     * target:定位对象，默认.as-u
     * align:位置，默认'left top'
     * width:宽度，默认300
     * @author kebai
     * @date 2016-7-12
     */
    that.dialogTipsText = function(objParent, obj, target, width, align) {
        objParent = objParent || '.as-grid';
        obj = obj || '.as-msgText';
        target = target || '.as-u';
        width = width || 300;
        align = align || 'left top';
        var $objParent = $(objParent);
        if ($objParent.length == '0') {
            return;
        }
        var box = '#content-' + obj.replace(/#|\./g, '');
        var dialogObj,
            $this, $target, $box,
            setT = null,
            showFlag = 0;
        //离开
        function fnOut() {
            showFlag = 0;
            clearTimeout(setT);
            setT = setTimeout(function() {
                if (dialogObj && !showFlag) {
                    dialogObj.close();
                }
            }, 200);
        }

        $objParent.off('mouseenter').on('mouseenter', obj, function() {
            clearTimeout(setT);
            showFlag = 1;
            $this = $(this);
            $target = $this.find(target).get(0);
            $this.attr("title", "");
            dialogObj = dialog({
                align: align,
                width: width,
                id: "as-msgText"
            });
            dialogObj.content($this.text());
            dialogObj.show($target);
            $box = $(box);
        });

        $objParent.off('mouseleave').on('mouseleave', obj, function() {
            fnOut();
            //进入对话框
            $box.hover(function() {
                showFlag = 1;
            }, function() {
                fnOut();
            });
        });

    };


    /** 
     * title: 身份证验证
     * idCardNo:身份证号码
     * @date 2017-3-1
     */
    that.checkIdCardNo = function(idCardNo) {
        var idCardNoUtil = {
            provinceAndCitys: {
                11: "北京",
                12: "天津",
                13: "河北",
                14: "山西",
                15: "内蒙古",
                21: "辽宁",
                22: "吉林",
                23: "黑龙江",
                31: "上海",
                32: "江苏",
                33: "浙江",
                34: "安徽",
                35: "福建",
                36: "江西",
                37: "山东",
                41: "河南",
                42: "湖北",
                43: "湖南",
                44: "广东",
                45: "广西",
                46: "海南",
                50: "重庆",
                51: "四川",
                52: "贵州",
                53: "云南",
                54: "西藏",
                61: "陕西",
                62: "甘肃",
                63: "青海",
                64: "宁夏",
                65: "新疆",
                71: "台湾",
                81: "香港",
                82: "澳门",
                91: "国外"
            },
            powers: ["7", "9", "10", "5", "8", "4", "2", "1", "6", "3", "7", "9",
                "10", "5", "8", "4", "2"
            ],

            parityBit: ["1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2"],

            genders: {
                male: "男",
                female: "女"
            },
            checkAddressCode: function(addressCode) {
                var check = /^[1-9]\d{5}$/.test(addressCode);
                if (!check)
                    return false;
                if (idCardNoUtil.provinceAndCitys[parseInt(addressCode.substring(0, 2))]) {
                    return true;
                } else {
                    return false;
                }
            },
            checkBirthDayCode: function(birDayCode) {
                var check = /^[1-9]\d{3}((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))$/
                    .test(birDayCode);
                if (!check)
                    return false;
                var yyyy = parseInt(birDayCode.substring(0, 4), 10);
                var mm = parseInt(birDayCode.substring(4, 6), 10);
                var dd = parseInt(birDayCode.substring(6), 10);
                var xdata = new Date(yyyy, mm - 1, dd);
                if (xdata > new Date()) {
                    return false; // 生日不能大于当前日期
                } else if ((xdata.getFullYear() == yyyy) && (xdata.getMonth() == mm - 1) && (xdata.getDate() == dd)) {
                    return true;
                } else {
                    return false;
                }
            },
            getParityBit: function(idCardNo) {
                var id17 = idCardNo.substring(0, 17);

                var power = 0;
                for (var i = 0; i < 17; i++) {
                    power += parseInt(id17.charAt(i), 10) * parseInt(idCardNoUtil.powers[i]);
                }

                var mod = power % 11;
                return idCardNoUtil.parityBit[mod];
            },
            checkParityBit: function(idCardNo) {
                var parityBit = idCardNo.charAt(17).toUpperCase();
                if (idCardNoUtil.getParityBit(idCardNo) == parityBit) {
                    return true;
                } else {
                    return false;
                }
            },
            checkIdCardNo: function(idCardNo) {
                // 15位和18位身份证号码的基本校验
                var check = /^\d{15}|(\d{17}(\d|x|X))$/.test(idCardNo);
                if (!check)
                    return false;
                // 判断长度为15位或18位
                if (idCardNo.length == 15) {
                    return idCardNoUtil.check15IdCardNo(idCardNo);
                } else if (idCardNo.length == 18) {
                    return idCardNoUtil.check18IdCardNo(idCardNo);
                } else {
                    return false;
                }
            },

            // 校验15位的身份证号码
            check15IdCardNo: function(idCardNo) {
                // 15位身份证号码的基本校验
                var check = /^[1-9]\d{7}((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))\d{3}$/
                    .test(idCardNo);
                if (!check)
                    return false;
                // 校验地址码
                var addressCode = idCardNo.substring(0, 6);
                check = idCardNoUtil.checkAddressCode(addressCode);
                if (!check)
                    return false;
                var birDayCode = '19' + idCardNo.substring(6, 12);
                // 校验日期码
                return idCardNoUtil.checkBirthDayCode(birDayCode);
            },

            // 校验18位的身份证号码
            check18IdCardNo: function(idCardNo) {
                // 18位身份证号码的基本格式校验
                var check = /^[1-9]\d{5}[1-9]\d{3}((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))\d{3}(\d|x|X)$/
                    .test(idCardNo);
                if (!check)
                    return false;
                // 校验地址码
                var addressCode = idCardNo.substring(0, 6);
                check = idCardNoUtil.checkAddressCode(addressCode);
                if (!check)
                    return false;
                // 校验日期码
                var birDayCode = idCardNo.substring(6, 14);
                check = idCardNoUtil.checkBirthDayCode(birDayCode);
                if (!check)
                    return false;
                // 验证校检码
                return idCardNoUtil.checkParityBit(idCardNo);
            }

        };
        return idCardNoUtil.checkIdCardNo(idCardNo);
    };


}