const resmRouter = [
  {
    path: '/resm/wholeRoad',
    name: 'wholeRoad',
    component: () => import('@/views/resm/wholeRoad')
  },
    {
        path: '/resm/roadResource',
        name: 'roadResource',
        component: () => import('@/views/resm/roadResource')
    }
];

export default resmRouter;