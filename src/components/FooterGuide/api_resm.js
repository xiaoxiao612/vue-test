const base_path = './resm';

const resm = {
  //  地图
  "resmMap": {
    "dev": `${base_path}/road.json`,
    "prod": `${base_path}/iusermanager.jsp?callMethod=searchUser`
  },

    // 获取组织树
    "roadLeftTree": {
        "dev": `${base_path}/roadLeftTree.json`,
        "prod": `${base_path}/iorganizationmanager.jsp?callMethod=getSubOrgList`
    },

    //路面资源
    "roadResource":{
      "dev":`${base_path}/roadResource.json`,
      "prod": `${base_path}/iusermanager.jsp?callMethod=searchUser`
    },
    //删除用户
    "deleteUser": {
        "dev": `${base_path}/roadResource.json`,
        "prod": `${base_path}/iusermanager.jsp?callMethod=deleteUser`
    },


    // 新增后保存
    "saveOrg": {
        "dev": `${base_path}/roadResource.json`,
        "prod": `${base_path}/iorganizationmanager.jsp?callMethod=saveOrg`
    },

}

export default resm;