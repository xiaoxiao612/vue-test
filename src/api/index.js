/**
 * Created by zhangxiaoxiao on 2020/2/10.
 * 包含N个接口请求函数模块
 * 函数返回值：promise对象
 */

import  ajax from './ajax'

export const reqAddress=(geohash => ajax(`/position/${geohash}`))

export const reqFoodsTypes=() => ajax(`/index_catagory`)

export const reqShopList=(longitude,latitude) =>ajax('/shops',{longitude,latitude})
